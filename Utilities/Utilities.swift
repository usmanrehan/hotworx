//
//  Utilities.swift
//  HotWorx
//
//  Created by Akber Sayni on 19/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import Foundation
import HealthKit

func computeDurationOfWorkout(withEvents workoutEvents: [HKWorkoutEvent]?, startDate: Date?, endDate: Date?) -> TimeInterval {
    var duration = 0.0
    
    if var lastDate = startDate {
        var paused = false
        
        if let events = workoutEvents {
            for event in events {
                switch event.type {
                case .pause:
                    duration += event.date.timeIntervalSince(lastDate)
                    paused = true
                    
                case .resume:
                    lastDate = event.date
                    paused = false
                    
                default:
                    continue
                }
            }
        }
        
        if !paused {
            if let end = endDate {
                duration += end.timeIntervalSince(lastDate)
            } else {
                duration += NSDate().timeIntervalSince(lastDate)
            }
        }
    }
    
    print("\(duration)")
    return duration
}

func format(energy: HKQuantity) -> String {
    return String(format: "%.1f Calories", energy.doubleValue(for: HKUnit.kilocalorie()))
}

func format(distance: HKQuantity) -> String {
    return String(format: "%.1f Meters", distance.doubleValue(for: HKUnit.meter()))
}

func format(duration: TimeInterval) -> String {
    let durationFormatter = DateComponentsFormatter()
    durationFormatter.unitsStyle = .positional
    durationFormatter.allowedUnits = [.second, .minute, .hour]
    durationFormatter.zeroFormattingBehavior = .pad
    
    if let string = durationFormatter.string(from: duration) {
        return string
    } else {
        return ""
    }
}

func format(activityType: HKWorkoutActivityType) -> String {
    let formattedType : String
    
    switch activityType {
    case .americanFootball:
        formattedType = "americanFootball"
        
    case .archery:
        formattedType = "archery"
        
    case .australianFootball:
        formattedType = "australianFootball"
        
    case .badminton:
        formattedType = "badminton"
        
    case .baseball:
        formattedType = "baseball"
        
    case .basketball:
        formattedType = "basketball"
        
    case .bowling:
        formattedType = "bowling"
        
    case .boxing: // See also HKWorkoutActivityTypeKickboxing.
        formattedType = "boxing"
        
    case .climbing:
        formattedType = "climbing"
        
    case .cricket:
        formattedType = "cricket"
        
    case .crossTraining: // Any mix of cardio and/or strength training. See also HKWorkoutActivityTypeCoreTraining and HKWorkoutActivityTypeFlexibility.
        formattedType = "crossTraining"
        
    case .curling:
        formattedType = "curling"
        
    case .cycling:
        formattedType = "cycling"
        
    case .dance:
        formattedType = "dance"
        
    case .elliptical:
        formattedType = "elliptical"
        
    case .equestrianSports: // Polo, Horse Racing, Horse Riding, etc.
        formattedType = "equestrianSports"
        
    case .fencing:
        formattedType = "fencing"
        
    case .fishing:
        formattedType = "fishing"
        
    case .functionalStrengthTraining: // Primarily free weights and/or body weight and/or accessories
        formattedType = "functionalStrengthTraining"
        
    case .golf:
        formattedType = "golf"
        
    case .gymnastics:
        formattedType = "gymnastics"
        
    case .handball:
        formattedType = "handball"
        
    case .hiking:
        formattedType = "hiking"
        
    case .hockey: // Ice Hockey, Field Hockey, etc.
        formattedType = "hockey"
        
    case .hunting:
        formattedType = "hunting"
        
    case .lacrosse:
        formattedType = "lacrosse"
        
    case .martialArts:
        formattedType = "martialArts"
        
    case .mindAndBody: // Qigong, meditation, etc.
        formattedType = "mindAndBody"
        
    case .paddleSports: // Canoeing, Kayaking, Outrigger, Stand Up Paddle Board, etc.
        formattedType = "paddleSports"
        
    case .play: // Dodge Ball, Hopscotch, Tetherball, Jungle Gym, etc.
        formattedType = "play"
        
    case .preparationAndRecovery: // Foam rolling, stretching, etc.
        formattedType = "preparationAndRecovery"
        
    case .racquetball:
        formattedType = "racquetball"
        
    case .rowing:
        formattedType = "rowing"
        
    case .rugby:
        formattedType = "rugby"
        
    case .running:
        formattedType = "running"
        
    case .sailing:
        formattedType = "sailing"
        
    case .skatingSports: // Ice Skating, Speed Skating, Inline Skating, Skateboarding, etc.
        formattedType = "skatingSports"
        
    case .snowSports: // Sledding, Snowmobiling, Building a Snowman, etc. See also HKWorkoutActivityTypeCrossCountrySkiing, HKWorkoutActivityTypeSnowboarding, and HKWorkoutActivityTypeDownhillSkiing.
        formattedType = "snowSports"
        
    case .soccer:
        formattedType = "soccer"
        
    case .softball:
        formattedType = "softball"
        
    case .squash:
        formattedType = "squash"
        
    case .stairClimbing: // See also HKWorkoutActivityTypeStairs and HKWorkoutActivityTypeStepTraining.
        formattedType = "stairClimbing"
        
    case .surfingSports: // Traditional Surfing, Kite Surfing, Wind Surfing, etc.
        formattedType = "surfingSports"
        
    case .swimming:
        formattedType = "swimming"
        
    case .tableTennis:
        formattedType = "tableTennis"
        
    case .tennis:
        formattedType = "tennis"
        
    case .trackAndField: // Shot Put, Javelin, Pole Vaulting, etc.
        formattedType = "trackAndField"
        
    case .traditionalStrengthTraining: // Primarily machines and/or free weights
        formattedType = "traditionalStrengthTraining"
        
    case .volleyball:
        formattedType = "volleyball"
        
    case .walking:
        formattedType = "walking"
        
    case .waterFitness:
        formattedType = "waterFitness"
        
    case .waterPolo:
        formattedType = "waterPolo"
        
    case .waterSports: // Water Skiing, Wake Boarding, etc.
        formattedType = "waterSports"
        
    case .wrestling:
        formattedType = "wrestling"
        
    case .yoga:
        formattedType = "yoga"
        
    case .barre: // HKWorkoutActivityTypeDanceInspiredTraining
        formattedType = "barre"
        
    case .coreTraining:
        formattedType = "coreTraining"
        
    case .crossCountrySkiing:
        formattedType = "crossCountrySkiing"
        
    case .downhillSkiing:
        formattedType = "downhillSkiing"
        
    case .flexibility:
        formattedType = "flexibility"
        
    case .highIntensityIntervalTraining:
        formattedType = "highIntensityIntervalTraining"
        
    case .jumpRope:
        formattedType = "jumpRope"
        
    case .kickboxing:
        formattedType = "kickboxing"
        
    case .pilates: // HKWorkoutActivityTypeDanceInspiredTraining
        formattedType = "pilates"
        
    case .snowboarding:
        formattedType = "snowboarding"
        
    case .stairs:
        formattedType = "stairs"
        
    case .stepTraining:
        formattedType = "stepTraining"
        
    case .wheelchairWalkPace:
        formattedType = "wheelchairWalkPace"
        
    case .wheelchairRunPace:
        formattedType = "wheelchairRunPace"
        
    default:
        formattedType = "other"
    }

    
    return formattedType
}

func format(locationType: HKWorkoutSessionLocationType) -> String {
    let formattedType : String
    
    switch locationType {
    case .indoor:
        formattedType = "Indoor"
        
    case .outdoor:
        formattedType = "Outdoor"
        
    case .unknown:
        formattedType = "Unknown"
    }
    
    return formattedType
}

func format(activityType: String) -> HKWorkoutActivityType {
    let formattedType: HKWorkoutActivityType
    
    switch activityType {
    case "americanFootball":
        formattedType = .americanFootball
        
    case "archery":
        formattedType = .archery
        
    case "australianFootball":
        formattedType = .australianFootball
        
    case "badminton":
        formattedType = .badminton
        
    case "baseball":
        formattedType = .baseball
        
    case "basketball":
        formattedType = .basketball
        
    case "bowling":
        formattedType = .bowling
        
    case "boxing": // See also HKWorkoutActivityTypeKickboxing.
        formattedType = .boxing
        
    case "climbing":
        formattedType = .climbing
        
    case "cricket":
        formattedType = .cricket
        
    case "crossTraining": // Any mix of cardio and/or strength training. See also HKWorkoutActivityTypeCoreTraining and HKWorkoutActivityTypeFlexibility.
        formattedType = .crossTraining
        
    case "curling":
        formattedType = .curling
        
    case "cycling":
        formattedType = .cycling
        
    case "dance":
        formattedType = .dance

    case "elliptical":
        formattedType = .elliptical
        
    case "equestrianSports": // Polo, Horse Racing, Horse Riding, etc.
        formattedType = .equestrianSports
        
    case "fencing":
        formattedType = .fencing
        
    case "fishing":
        formattedType = .fishing
        
    case "functionalStrengthTraining": // Primarily free weights and/or body weight and/or accessories
        formattedType = .functionalStrengthTraining
        
    case "golf":
        formattedType = .golf
        
    case "gymnastics":
        formattedType = .gymnastics
        
    case "handball":
        formattedType = .handball
        
    case "hiking":
        formattedType = .hiking
        
    case "hockey": // Ice Hockey, Field Hockey, etc.
        formattedType = .hockey
        
    case "hunting":
        formattedType = .hunting
        
    case "lacrosse":
        formattedType = .lacrosse
        
    case "martialArts":
        formattedType = .martialArts
        
    case "mindAndBody": // Qigong, meditation, etc.
        formattedType = .mindAndBody
        
    case "paddleSports": // Canoeing, Kayaking, Outrigger, Stand Up Paddle Board, etc.
        formattedType = .paddleSports
        
    case "play": // Dodge Ball, Hopscotch, Tetherball, Jungle Gym, etc.
        formattedType = .play
        
    case "preparationAndRecovery": // Foam rolling, stretching, etc.
        formattedType = .preparationAndRecovery
        
    case "racquetball":
        formattedType = .racquetball
        
    case "rowing":
        formattedType = .rowing
        
    case "rugby":
        formattedType = .rugby
        
    case "running":
        formattedType = .running
        
    case "sailing":
        formattedType = .sailing
        
    case "skatingSports": // Ice Skating, Speed Skating, Inline Skating, Skateboarding, etc.
        formattedType = .skatingSports
        
    case "snowSports": // Sledding, Snowmobiling, Building a Snowman, etc. See also HKWorkoutActivityTypeCrossCountrySkiing, HKWorkoutActivityTypeSnowboarding, and HKWorkoutActivityTypeDownhillSkiing.
        formattedType = .snowSports
        
    case "soccer":
        formattedType = .soccer
        
    case "softball":
        formattedType = .softball
        
    case "squash":
        formattedType = .squash
        
    case "stairClimbing": // See also HKWorkoutActivityTypeStairs and HKWorkoutActivityTypeStepTraining.
        formattedType = .stairClimbing
        
    case "surfingSports": // Traditional Surfing, Kite Surfing, Wind Surfing, etc.
        formattedType = .surfingSports
        
    case "swimming":
        formattedType = .swimming
        
    case "tableTennis":
        formattedType = .tableTennis
        
    case "tennis":
        formattedType = .tennis
        
    case "trackAndField": // Shot Put, Javelin, Pole Vaulting, etc.
        formattedType = .trackAndField
        
    case "traditionalStrengthTraining": // Primarily machines and/or free weights
        formattedType = .traditionalStrengthTraining
        
    case "volleyball":
        formattedType = .volleyball
        
    case "walking":
        formattedType = .walking
        
    case "waterFitness":
        formattedType = .waterFitness
        
    case "waterPolo":
        formattedType = .waterPolo
        
    case "waterSports": // Water Skiing, Wake Boarding, etc.
        formattedType = .waterSports
        
    case "wrestling":
        formattedType = .wrestling
        
    case "yoga":
        formattedType = .yoga
        
    case "barre": // HKWorkoutActivityTypeDanceInspiredTraining
        formattedType = .barre
    
    case "coreTraining":
        formattedType = .coreTraining
    
    case "crossCountrySkiing":
        formattedType = .crossCountrySkiing
    
    case "downhillSkiing":
        formattedType = .downhillSkiing
    
    case "flexibility":
        formattedType = .flexibility
    
    case "highIntensityIntervalTraining":
        formattedType = .highIntensityIntervalTraining
    
    case "jumpRope":
        formattedType = .jumpRope
    
    case "kickboxing":
        formattedType = .kickboxing
    
    case "pilates": // HKWorkoutActivityTypeDanceInspiredTraining
        formattedType = .pilates
    
    case "snowboarding":
        formattedType = .snowboarding
    
    case "stairs":
        formattedType = .stairs
    
    case "stepTraining":
        formattedType = .stepTraining
    
    case "wheelchairWalkPace":
        formattedType = .wheelchairWalkPace
    
    case "wheelchairRunPace":
        formattedType = .wheelchairRunPace
    
    default:
        formattedType = .other
    }
    
    return formattedType    
}
