/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 Interface controller for the active workout screen.
 */

import WatchKit
import Foundation
import HealthKit
import WatchConnectivity

class WorkoutInterfaceController: WKInterfaceController, HKWorkoutSessionDelegate, SessionCommands {

    @IBOutlet var workoutLabel: WKInterfaceLabel!
    @IBOutlet var durationLabel: WKInterfaceLabel!
    @IBOutlet var caloriesLabel: WKInterfaceLabel!
    @IBOutlet var doneButton: WKInterfaceButton!

    // MARK: Properties
    let healthStore = HKHealthStore()
    var workoutSession: HKWorkoutSession!
    var startDate: Date?
    var endDate: Date?
    var activeDataQueries = [HKQuery]()
    
    var timer : Timer?
    var workoutDuration: TimeInterval = 60 * 60 // default workout time
    var sessionStartedTimeInterval: TimeInterval!
    var burnedEnergy = HKQuantity(unit: HKUnit.kilocalorie(), doubleValue: 0)
	
	var caloriesBurnt: Double = 0.0

    // Context == nil: the fist-time loading, load pages with reloadRootController then
    // Context != nil: Loading the pages, save the controller instances so that we can
    // swtich pages more smoothly.
    //
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        if let configuration = context as? HKWorkoutConfiguration {
            self.startDate = Date()
            self.initWorkoutSession(configuration)
            self.addNotificationObservers()
        }
    }
    
    private func initWorkoutSession(_ configuration: HKWorkoutConfiguration) {
        do {
            if #available(watchOSApplicationExtension 5.0, *) {
                self.workoutSession = try HKWorkoutSession(healthStore: healthStore, configuration: configuration)
                self.workoutSession?.delegate = self
            } else {
                // Fallback on earlier versions
                self.workoutSession = try HKWorkoutSession(configuration: configuration)
                self.workoutSession.delegate = self
            }
			
            self.updateWorkoutLabel(with: format(activityType: configuration.activityType))
            
        } catch let error as NSError {
            fatalError("*** Unable to create the workout session: \(error.localizedDescription) ***")
        }
    }
    
    private func startWorkoutSession() {
        // Set activity start date
        self.startDate = Date()
        self.sessionStartedTimeInterval = Date.timeIntervalSinceReferenceDate
        
        if let session = self.workoutSession {
            if #available(watchOSApplicationExtension 5.0, *) {
                session.startActivity(with: self.startDate)
            } else {
                // Fallback on earlier versions
                self.healthStore.start(session)
            }
        }
        
        self.startTimer()
		//self.startCaloriesTimer()
        self.startAccumulatingData(startDate: self.startDate!)
        self.sendStartWorkoutSessionMessage()
    }
    
    private func stopWorkoutSession() {
        // Set activity end date
        self.endDate = Date()
        if let session = self.workoutSession {
            if #available(watchOSApplicationExtension 5.0, *) {
                session.stopActivity(with: self.endDate)
                session.end()
            } else {
                // Fallback on earlier versions
                self.healthStore.end(session)
            }
        }
        
        self.stopTimer()
        self.stopAccumulatingData()
    }
    
    private func cancelWorkoutSession() {
        if self.timer != nil {
            self.stopWorkoutSession()
        }
		
        WKInterfaceController.reloadRootControllers(withNames: ["ConfigurationInterfaceController"], contexts: nil)
    }
    
    private func addNotificationObservers() {
        // Install notification observer.
        //
        NotificationCenter.default.addObserver(
            self, selector: #selector(type(of: self).dataDidFlow(_:)),
            name: .dataDidFlow, object: nil
        )
        NotificationCenter.default.addObserver(
            self, selector: #selector(type(of: self).activationDidComplete(_:)),
            name: .activationDidComplete, object: nil
        )
        NotificationCenter.default.addObserver(
            self, selector: #selector(type(of: self).reachabilityDidChange(_:)),
            name: .reachabilityDidChange, object: nil
        )
    }
    
    private func startAccumulatingData(startDate: Date) {
        self.startQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)
    }

    private func stopAccumulatingData() {
        // Stop all active data queries
        for query in self.activeDataQueries {
            self.healthStore.stop(query)
        }
        
        self.activeDataQueries.removeAll()
    }
    
    private func setBurnedEnergy(calories: Double) {
        self.burnedEnergy = HKQuantity(unit: HKUnit.kilocalorie(), doubleValue: calories)
    }
    
    private func getBurnedEnergy() -> Double {
        return self.burnedEnergy.doubleValue(for: HKUnit.kilocalorie())
    }
    
    private func reloadController() {
        WKInterfaceController.reloadRootControllers(withNames: ["ConfigurationInterfaceController"], contexts: nil)
    }
    
    private func sendStartWorkoutSessionMessage() {
        let message = [PayloadKey.command: PayloadKey.startWatchWorkout]
        self.sendMessage(message)
    }
}

//MARK:- Timer methods
extension WorkoutInterfaceController {
    private func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(firedTimer), userInfo: nil, repeats: true)
    }
    
    private func stopTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @objc private func firedTimer() {
        let currentTimeInterval = Date.timeIntervalSinceReferenceDate
        let elapsedTimeInterval = currentTimeInterval - sessionStartedTimeInterval
        
        if elapsedTimeInterval >= self.workoutDuration {
            self.stopWorkoutSession()
        } else {
            self.updateDurationLabel(with: self.workoutDuration - elapsedTimeInterval)
        }
		
		// Send calories values to phone app
		self.sendCalories(caloriesBurnt)

		// in case app didn't start timer
		self.sendStartWorkoutSessionMessage()
    }
}

//MARK:- Update UI methods
extension WorkoutInterfaceController {
    private func updateWorkoutLabel(with title: String) {
        self.workoutLabel.setText(title)
    }
    
    private func updateDurationLabel(with duration: Double) {
        self.durationLabel.setText(format(duration: duration))
    }
    
    private func updateCaloriesLabel(with calories: Double) {
		self.caloriesBurnt = calories
        self.caloriesLabel.setText(String(format: "%.2f Calories", calories))
    }
	
    private func sendCalories(_ calories: Double) {
        let message = [PayloadKey.activeEnergyBurned: calories]
        self.sendMessage(message)
    }
}

extension WorkoutInterfaceController {
    @IBAction func didTapDoneButton() {
        self.stopWorkoutSession()
        WKInterfaceController.reloadRootControllers(withNames: ["ConfigurationInterfaceController"], contexts: nil)
    }
}

// MARK:- Data Queries
extension WorkoutInterfaceController {
    private func startQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier) {
        let datePredicate = HKQuery.predicateForSamples(withStart: self.startDate, end: nil, options: .strictStartDate)
        let devicePredicate = HKQuery.predicateForObjects(from: [HKDevice.local()])
        let queryPredicate = NSCompoundPredicate(andPredicateWithSubpredicates:[datePredicate, devicePredicate])
        
        let updateHandler: ((HKAnchoredObjectQuery, [HKSample]?, [HKDeletedObject]?, HKQueryAnchor?, Error?) -> Void) = { query, samples, deletedObjects, queryAnchor, error in
            self.process(samples: samples, quantityTypeIdentifier: quantityTypeIdentifier)
        }
        
        let query = HKAnchoredObjectQuery(type: HKObjectType.quantityType(forIdentifier: quantityTypeIdentifier)!,
                                          predicate: queryPredicate,
                                          anchor: nil,
                                          limit: HKObjectQueryNoLimit,
                                          resultsHandler: updateHandler)
        
        query.updateHandler = updateHandler
        self.healthStore.execute(query)
        
        self.activeDataQueries.append(query)
    }
    
    private func process(samples: [HKSample]?, quantityTypeIdentifier: HKQuantityTypeIdentifier) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            if let quantitySamples = samples as? [HKQuantitySample] {
                for sample in quantitySamples {
                    if quantityTypeIdentifier == HKQuantityTypeIdentifier.activeEnergyBurned {
                        let newKCal = sample.quantity.doubleValue(for: HKUnit.kilocalorie())
                        strongSelf.setBurnedEnergy(calories: strongSelf.getBurnedEnergy() + newKCal)
                        strongSelf.updateCaloriesLabel(with: strongSelf.getBurnedEnergy())
                    }
                }
            }
        }
    }
}

extension WorkoutInterfaceController {
    public func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
        DispatchQueue.main.async {
            print("HKWorkoutSession state changed from \(fromState.rawValue) to \(toState.rawValue)")
            switch toState {
            case .running:
                print("Workout session running")
            case .ended:
                print("Workout session ended")
            case .stopped:
                print("Workout session stopped")
            default:
                print("default")
            }
        }
    }
    
    public func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        print("HKWorkoutSession failed with error \(error.localizedDescription)")
    }
    
    public func workoutSession(_ workoutSession: HKWorkoutSession, didGenerate event: HKWorkoutEvent) {
        print("HKWorkoutSession generate event \(event)")
    }
}

//MARK:- Notification Methods
extension WorkoutInterfaceController {
    // .dataDidFlow notification handler. Update the UI based on the command status.
    //
    @objc
    func dataDidFlow(_ notification: Notification) {
        guard let message = notification.object as? [String: Any] else { return }

        if let command = message[PayloadKey.command] as? String {
            switch command {
            case PayloadKey.cancelWorkout:
                self.cancelWorkoutSession()
                break
            case PayloadKey.recordCalories:
                self.reloadController()
                break
            case PayloadKey.updateCalories:
                self.sendCalories(caloriesBurnt)
                break
            default:
                print("Unhandle command: \(command)")
                break
            }
        } else {
            if let workoutDuration = message[PayloadKey.startWorkout] as? Double {
                self.workoutDuration = workoutDuration
                self.startWorkoutSession()
            }
        }
    }
    
    // .activationDidComplete notification handler.
    //
    @objc
    func activationDidComplete(_ notification: Notification) {
        print("\(#function): activationState:\(WCSession.default.activationState.rawValue)")
    }
    
    // .reachabilityDidChange notification handler.
    //
    @objc
    func reachabilityDidChange(_ notification: Notification) {
        print("\(#function): isReachable:\(WCSession.default.isReachable)")
    }
}
