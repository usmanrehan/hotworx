//
//  ParentConnector.swift
//  HotWorxWatch Extension
//
//  Created by Akber Sayni on 19/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import WatchConnectivity

protocol ParentControllerDelegate {
    func didReceivedMessage(_ message: [String: Any])
}

class ParentConnector : NSObject, WCSessionDelegate {
    // MARK: Properties
    var delegate: ParentControllerDelegate?
    
    //var wcSession: WCSession?
    
    var statesToSend = [String]()
    var caloriesToSend = [Double]()
    
    
    // MARK: Utility methods
    func activateSession() {
        let session = WCSession.default
        session.delegate = self
        session.activate()
    }
    
    func send(state: String) {
        let session = WCSession.default
        if session.activationState == .activated {
            session.sendMessage(["State": ""],
                                replyHandler: { (dict) in
                                    print(dict)
            }) { (error) in
                print(error.localizedDescription)
            }            
            //session.sendMessage(["State": state], replyHandler: nil, errorHandler: nil)
        } else {
            statesToSend.append(state)
            self.activateSession()
        }
        
//        if let session = wcSession {
//            if session.isReachable {
//                session.sendMessage(["State": state], replyHandler: nil)
//            }
//        } else {
//            WCSession.default.delegate = self
//            WCSession.default.activate()
//            statesToSend.append(state)
//        }
    }
    
    func send(calories: Double) {
        let session = WCSession.default
        if session.activationState == .activated {
            session.sendMessage(["State": ""],
                                replyHandler: { (dict) in
                                    print(dict)
            }) { (error) in
                print(error.localizedDescription)
            }

            //session.sendMessage(["Calories": calories], replyHandler: nil, errorHandler: nil)
        } else {
            caloriesToSend.append(calories)
            self.activateSession()
        }
        
//        if let session = wcSession {
//            if session.isReachable {
//                session.sendMessage(["Calories": calories], replyHandler: nil)
//            }
//        } else {
//            let wcSession = WCSession.default
//            wcSession.delegate = self
//            caloriesToSend.append(calories)
//
//            if wcSession.activationState == .activated {
//                self.wcSession = wcSession
//                sendPending()
//            } else {
//                wcSession.activate()
//            }
//        }
    }
    
    private func sendPending() {
        let session = WCSession.default
        if session.activationState == .activated {
            for state in statesToSend {
                session.sendMessage(["State": ""],
                                    replyHandler: { (dict) in
                                        print(dict)
                }) { (error) in
                    print(error.localizedDescription)
                }

//                session.sendMessage(["State": state], replyHandler: nil) { (error) in
//                    print(error.localizedDescription)
//                }
            }
            
            for calories in caloriesToSend {
                //session.sendMessage(["Calories": calories], replyHandler: nil)
                session.sendMessage(["State": ""],
                                    replyHandler: { (dict) in
                                        print(dict)
                }) { (error) in
                    print(error.localizedDescription)
                }

            }
            
            statesToSend.removeAll()
            caloriesToSend.removeAll()
        }
        
//        if let session = wcSession {
//            if session.isReachable {
//                for state in statesToSend {
//                    session.sendMessage(["State": state], replyHandler: nil)
//                }
//
//                for calories in caloriesToSend {
//                    session.sendMessage(["Calories": calories], replyHandler: nil)
//                }
//
//                statesToSend.removeAll()
//                caloriesToSend.removeAll()
//            }
//        }
    }
    
    // MARK : WCSessionDelegate
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if activationState == .activated {
            //wcSession = session
            sendPending()
        }
    }
    
//    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
//        if self.delegate != nil {
//            self.delegate?.didReceivedMessage(message)
//        }
//    }
//
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        if self.delegate != nil {
            self.delegate?.didReceivedMessage(message)
        }
    }
}
