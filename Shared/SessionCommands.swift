//
//  SessionCommands.swift
//  HotWorx
//
//  Created by Akber Sayni on 14/12/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import WatchConnectivity

struct PayloadKey {
    static let command = "command"
    static let startWorkout = "startWorkout"
    static let stopWorkout = "stopWorkout"
    static let cancelWorkout = "cancelWorkout"
    static let recordCalories = "recordCalories"
    static let updateCalories = "updateCalories"
    static let workoutStateRunning = "workoutStateRunning"
    static let workoutDuration = "workoutDuration"
    static let activeEnergyBurned = "activeEnergyBurned"
    static let startWatchWorkout = "startWatchWorkout"
}

// Define an interface to wrap Watch Connectivity APIs and
// bridge the UI. Shared by the iOS app and watchOS app.
//
protocol SessionCommands {
    func sendMessage(_ message: [String: Any])
}

// Implement the commands. Every command handles the communication and notifies clients
// when WCSession status changes or data flows. Shared by the iOS app and watchOS app.
//
extension SessionCommands {
    // Send a message if the session is activated and update UI with the command status.
    //
    func sendMessage(_ message: [String: Any]) {
        guard WCSession.default.activationState == .activated else {
            //return handleSessionUnactivated(with: commandStatus)
            return handleSessionUnactivated()
        }
        
        WCSession.default.sendMessage(message, replyHandler: { replyMessage in
            print("replyHandler: \(replyMessage)")
        }, errorHandler: { error in
            print("error: \(error.localizedDescription)")
        })
        
        postNotificationOnMainQueueAsync(name: .dataDidFlow, object: message)
    }

    private func postNotificationOnMainQueueAsync(name: NSNotification.Name, object: Any? = nil) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: name, object: object)
        }
    }
    
    // Handle the session unactived error. WCSession commands require an activated session.
    //
    private func handleSessionUnactivated() {
        print("Failure message")
    }
}
