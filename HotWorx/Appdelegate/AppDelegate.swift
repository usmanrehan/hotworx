//
//  AppDelegate.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 29/06/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import RESideMenu
import RealmSwift
import IQKeyboardManagerSwift
import UserNotifications
import HealthKit
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    private lazy var sessionDelegater: SessionDelegater = {
        return SessionDelegater()
    }()

    static let shared: AppDelegate = UIApplication.shared.delegate as! AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        // Trigger WCSession activation at the early phase of app launching.
        //
        self.initWCSession()
        
        UNUserNotificationCenter.current().delegate = self
        self.requestLocalNotificationAuthorization()

        IQKeyboardManager.shared.enable = true
        
        self.processRealmMigration()
        self.registerForPushNotifications()
        
        self.changeRootViewController()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        let notification = Notification(
            name: Notification.Name(rawValue: NotificationConstants.launchNotification),
            object:nil,
            userInfo:[UIApplicationLaunchOptionsKey.url:url])
        
        NotificationCenter.default.post(notification)
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    private func initWCSession() {
        if WCSession.isSupported() {
            WCSession.default.delegate = sessionDelegater
            WCSession.default.activate()
        }
    }
    
    private func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }

    private func requestLocalNotificationAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            if granted {
                print("Request authorization granted")
            }
        }
    }
    
    private func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async(execute: {
                UIApplication.shared.registerForRemoteNotifications()
            })
        }
    }
    
    private func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        AppStateManager.sharedInstance.deviceToken = token
    }
    
    private func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
}

//MARK:- Application Flow
extension AppDelegate {
    
    public func showTutorial() {
        let storyboard = AppStoryboard.Login.instance
        let navigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Tutorial"))
        navigationController.navigationBar.isHidden = true
        self.window?.rootViewController = nil
        UIView.transition(with: self.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            self.window?.rootViewController = navigationController
        }, completion: nil)
    }
    
    public func showSignIn() {
        let storyboard = AppStoryboard.Login.instance
        let navigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "SignIn"))
        navigationController.navigationBar.isHidden = true
        UIView.transition(with: self.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            self.window?.rootViewController = navigationController
        }, completion: nil)
    }
    
    private func showHome() {
        let storyboard = AppStoryboard.Home.instance
        let homeController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Home"))
        let sideMenuController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "SideMenu"))
        
        homeController.navigationBar.isHidden = true
        sideMenuController.navigationBar.isHidden = true
        
        // Create side menu controller
        let RESideMenuViewController = RESideMenu(contentViewController: homeController, leftMenuViewController: sideMenuController, rightMenuViewController: nil)
        RESideMenuViewController?.delegate = self
        // Make it a root controller
        RESideMenuViewController?.panGestureEnabled = false
        UIView.transition(with: self.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            self.window?.rootViewController = RESideMenuViewController
        }, completion: nil)
    }
    
    public func changeRootViewController() {
        if AppStateManager.sharedInstance.isUserLoggedIn() {
            self.showHome()
        }
        else {
            self.showTutorial()
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}

extension AppDelegate: RESideMenuDelegate {
    public func sideMenu(_ sideMenu: RESideMenu!, willShowMenuViewController menuViewController: UIViewController!) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationConstants.WILL_SHOW_SIDE_MENU), object: nil)
    }
}

//MARK:- Realm Migration
extension AppDelegate{
    private func processRealmMigration() {
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = config
    }
}
