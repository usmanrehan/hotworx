//
//  AppStateManager.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class AppStateManager: NSObject {
    
    static let sharedInstance = AppStateManager()
    var loggedInUser: User!
    var realm: Realm!
    var deviceToken = ""
    
    var parentActivityId = "0"
    var activityId = "0"
    var arrActiveSessions: [SessionsData]?
    
    override init() {
        super.init()
        if(!(realm != nil)){
            realm = try! Realm()
        }
        
        loggedInUser = realm.objects(User.self).first
    }
    
    func isUserLoggedIn() -> Bool{
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            
            return true
        }
        return false
    }
	
    func createUser(with userModel: User) {
        self.loggedInUser = userModel
        
        try! Global.APP_REALM?.write(){
            Global.APP_REALM?.add(self.loggedInUser, update: true)
        }
        
        print(AppStateManager.sharedInstance.loggedInUser)
    }
    
    func logoutUser() {
        try! Global.APP_REALM?.write() {
            Global.APP_REALM?.delete(self.loggedInUser)
            self.loggedInUser = nil
        }
        
        AppDelegate.shared.changeRootViewController()
    }
	
    func getActionSession() -> SessionsData? {
        if let activeSession = Global.APP_REALM?.objects(SessionsData.self).last {
            return activeSession
        }
        
        return nil
    }
    
    func setActiveSession(_ activeSession: SessionsData, watchType: Int, sessionStartedTimeInterval: TimeInterval) {
        try! Global.APP_REALM?.write {
            activeSession.watchType = watchType
            activeSession.activityId = self.activityId
            activeSession.parentActivityId = self.parentActivityId
            activeSession.sessionStartedTimeInterval = sessionStartedTimeInterval

            Global.APP_REALM?.add(activeSession, update: true)
        }
    }
    
    func deleteActiveSession(_ activeSession: SessionsData) {
        try! Global.APP_REALM?.write {
            Global.APP_REALM?.delete(activeSession)
        }
    }
	
	func getParentActivityId() -> String? {
		return UserDefaults.standard.string(forKey: "ParentActivityId")
	}
	
	func setParentActivityId(_ activityId: String) {
		UserDefaults.standard.set(activityId, forKey: "ParentActivityId")
	}
	
	func removeParentActivityId() {
		UserDefaults.standard.removeObject(forKey: "ParentActivityId")
	}
}
