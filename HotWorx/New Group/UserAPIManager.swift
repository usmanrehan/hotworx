//
//  UserAPIManager.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    //MARK:- SIGN IN
    func signIn(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:"", params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    //MARK:- LOGOUT
    func logout(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.Logout.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- leaderboardGlobal
    func leaderboardGlobal(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetGlobalLeaderboard.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- leaderboardLocal
    func leaderboardLocal(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetLocalLeaderboard.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getPrivacyContent
    func getPrivacyContent(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetPrivacyContent.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getPrivacyContent
    func getHelpContent(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetPrivacyContent.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- addFeedback
    func addFeedback(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.AddFeedback.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getTodaysSessions
    func getTodaysSessions(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetTodaysSessions.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getSummary
    func getSummary(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetSummary.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getFinalSummary
    func getFinalSummary(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetFinalSummary.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- startSession
    func startSession(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.AddStartSession.rawValue)
        self.postRequestWithMultipart(route: route!, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- AddActiveSession
    func updateSession(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.AddActiveSession.rawValue)
        self.postRequestWithMultipart(route: route!, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- AddAfterHourSession
    func updateAfterHourSession(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.AddAfterHour.rawValue)
        self.postRequestWithMultipart(route: route!, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- cancelSession
    func cancelSession(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.CancelActivity.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- updateProfile
    func updateProfile(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.UpdateProfile.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getUserProfile
    func getUserProfile(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.viewProfile.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getOnlineAppointmentLink
    func getOnlineAppointmentLink(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetOnlineAppointmentLink.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getAllRewards
    func getAllRewards(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetAllRewards.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getCaloriesStats
    func getCaloriesStats(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route:WebRoute.GetCaloriesStats.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- viewUserActivityYearly
    func viewUserActivityYearly(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route:WebRoute.viewUserActivityYearly.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- getAllRewards
    func getNinetyDaysSummary(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetNinetyDaysSummary.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- GetSummaryThirtyDays
    func getSummaryThirtyDays(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetSummaryThirtyDays.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- GetBodyFatGraph
    func getBodyFatGraph(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetBodyFatGraph.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- UploadImage
    func uploadImage(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.UploadImage.rawValue)
        self.postRequestWithMultipart(route: route!, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- RecordBodyFat
    func recordBodyFat(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.RecordBodyFat.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- GetRedemptionStatus
    func getRedemptionStatus(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetRedemptionStatus.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- RedeemReward
    func redeemReward(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.RedeemReward.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- ShareFacebookActivity
    func shareFacebookActivity(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.ShareFacebookActivity.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    //MARK:- Get Franchise Listing
    func getFranchiseList(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetFranchiseList.rawValue)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    //MARK:- Get Franchise Summary
    func getFranchiseSummary(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetFranchiseSummary.rawValue)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    //MARK:- Get Franchise Leaderboard
    func getFranchiseLeaderbooard(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetFranchiseLeaderboard.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    //MARK:- Get Franchise Users Leaderboard
    func getFranchiseUsersLeaderbooard(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.GetFranchiseUsersLeaderboard.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: true)
    }
}
