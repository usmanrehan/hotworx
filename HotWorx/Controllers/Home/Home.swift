//
//  Home.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 01/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import PieCharts
import ObjectMapper
import SwiftDate

struct TwoStrings {
    var string1 = ""
    var string2 = ""
}

class Home: BaseController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgGymCover: UIImageView!
    @IBOutlet weak var lblClassesTitle: UILabel!
    @IBOutlet var dateButtons: [UIButton]!
    @IBOutlet weak var pieChartBG: UIView!
    @IBOutlet weak var pieChart: PieChart!
    @IBOutlet weak var hitCaloriesLabel: UILabel!
    @IBOutlet weak var isometricCalorieLabel: UILabel!
    @IBOutlet weak var oneHourAferBurnLabel: UILabel!

    var arrClassesCompleted = [ClassesCompleted]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.addNotificationObserver()
        self.setupData()
		self.checkActiveSession()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getSummary()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
		
        self.sideMenuViewController?.hideViewController()
    }
	
	private func checkActiveSession() {
		if let activeSession = AppStateManager.sharedInstance.getActionSession() {
			// Restore last activity and parent activity id
			AppStateManager.sharedInstance.activityId = activeSession.activityId ?? "0"
			AppStateManager.sharedInstance.parentActivityId = activeSession.parentActivityId ?? "0"
			self.pushToStartSession(activeSession, isSessionResumed: true)
		}
	}

    private func addNotificationObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.pushToOneHourAfterBurnSession(notification:)),
                                               name: Notification.Name(rawValue: NotificationConstants.START_ONE_HOUR_AFTER_BURN),
                                               object: nil)
    }
    
    private func setupData() {
        for (index, button) in self.dateButtons.enumerated() {
            let date = DateInRegion() - index.days
            let dateString = date.toString(.custom("MMM dd, yyyy"))
            button.setTitle(dateString, for: .normal)
        }
    }
	
	private func resumeSessionPopup(_ parentActivityId: String) {
		let alertController = UIAlertController(title: "Confirmation", message: "Your previous workout session was not completed. Would you like to resume old session?", preferredStyle: .alert)
		
		let actionYes = UIAlertAction(title: "YES", style: .default) { (action) in
			
			// Set previous stored parent activity Id
			AppStateManager.sharedInstance.parentActivityId = parentActivityId
			
			self.pushToSessions()
		}
		
		let actionNo = UIAlertAction(title: "NO", style: .default) { (action) in
			// Clear stored data
			self.cancelSession(parentActivityId)
			
			AppStateManager.sharedInstance.parentActivityId = "0"
			AppStateManager.sharedInstance.activityId = "0"
			AppStateManager.sharedInstance.arrActiveSessions = nil
			
			// Remove parent Id from user defaults
			AppStateManager.sharedInstance.removeParentActivityId()

			self.pushToSessions()
		}
		
		alertController.addAction(actionYes)
		alertController.addAction(actionNo)
		
		self.present(alertController, animated: true, completion: nil)
	}
    
    @IBAction func onBtnStartSession(_ sender: UIButton) {
		if let parentActivityId = AppStateManager.sharedInstance.getParentActivityId() {
			// ask user to resume previous session
			self.resumeSessionPopup(parentActivityId)
		} else {
			// Start a new session
			self.pushToSessions()
		}
    }
    
    @IBAction func onBtnSelectDate(_ sender: UIButton) {
        for button in self.dateButtons {
            button.isSelected = false
        }
        
        sender.isSelected = true
        let dateString = sender.titleLabel?.text
        self.getSummary(with: dateString!)
    }
}

//MARK:- Helper Methods
extension Home{
    private func setPieChart(arrSummary: [Summary]) {
        if let summary = arrSummary.first {
            self.pieChart.isUserInteractionEnabled = false
            self.pieChart.innerRadius = self.pieChart.frame.height * 0.275
            self.pieChart.outerRadius = self.pieChart.frame.height * 0.425
            self.pieChartBG.isHidden = false
            self.pieChart.models = [
                PieSliceModel(value: Double(summary.isometricCalories ?? "0")!, color: ColorConstants.APP_COLOR),
                PieSliceModel(value: Double(summary.hiitCalories ?? "0")!, color: ColorConstants.YELLOW),
                PieSliceModel(value: Double(summary.afterBurn ?? "0")!, color: ColorConstants.BLUE)
            ]
        }
    }
    
    private func updateLabels(arrSummary: [Summary]) {
        if let summary = arrSummary.first {
            self.hitCaloriesLabel.text = summary.hiitCalories
            self.oneHourAferBurnLabel.text = summary.afterBurn
            self.isometricCalorieLabel.text = summary.isometricCalories
        }
    }
    
    private func pushToSessions(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Sessions")
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func pushToStartSession(_ activeSession: SessionsData, isSessionResumed: Bool) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "StartSessions") as? StartSessions {
            controller.isSessionResumed = isSessionResumed
            controller.activeSession = activeSession
            controller.watchType = WatchType(rawValue: activeSession.watchType)!
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc private func pushToOneHourAfterBurnSession(notification: NSNotification) {
        // Create after one hour session
        let activeSession = SessionsData()
        activeSession.type = "AFTER BURN"
        activeSession.appleWatchType = "other"
        activeSession.duration = Constants.AFTER_BURN_TIME_OUT
        activeSession.isAfterBurn = true

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "StartSessions") as! StartSessions
        controller.activeSession = activeSession
        
        if let watchType = notification.userInfo?["watchType"] as? WatchType {
            controller.watchType = watchType
        }

        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension Home: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrClassesCompleted.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTVC", for: indexPath) as! HomeTVC
        cell.selectionStyle = .none
        cell.setData(data: self.arrClassesCompleted[indexPath.row])
        if indexPath.row == self.arrClassesCompleted.count - 1 {
            cell.periodBottonConstraint.constant = 30
        }
        return cell
    }
}

extension Home: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = CGFloat(60)
        return height
    }
}

//MARK:- Webservices
extension Home{
    private func getSummary() {
        Utility.showLoader()
        let params : [String:Any] = [
            "user_id":AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.getSummary(params: params, success: { (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            self.onSuccessResponse(responseObject)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func getSummary(with dateString: String) {
        Utility.showLoader()
        let params : [String:Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
            "date": dateString
        ]
        APIManager.sharedInstance.usersAPIManager.getSummary(params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.onSuccessResponse(responseObject)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func onSuccessResponse(_ responseObject: [String: Any]) {
        if let summary = responseObject["summary"] as? [[String: Any]] {
            let arrSummary = Mapper<Summary>().mapArray(JSONArray: summary)
            self.updateLabels(arrSummary: arrSummary)
            self.setPieChart(arrSummary: arrSummary)
        }

        self.arrClassesCompleted = Array()
        
        if let completedClasses = responseObject["classes_completed"] as? [[String: Any]] {
            let arrClassesCompleted = Mapper<ClassesCompleted>().mapArray(JSONArray: completedClasses)
            self.arrClassesCompleted = arrClassesCompleted
        }
        
        self.tableView.reloadData()
    }
	
	private func cancelSession(_ parentActivityId: String) {
		let params : [String: Any] = [
			"user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
			"parent_activity_id": parentActivityId
		]
		APIManager.sharedInstance.usersAPIManager.cancelSession(params: params, success: { (responseObject) in
			print("session cancelled")
		}) { (error) in
			print(error.localizedDescription)
		}
	}
}
