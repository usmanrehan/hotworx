//
//  SideMenu.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 01/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class SideMenu: BaseController {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblRecentCalories: UILabel!
    @IBOutlet weak var lblTotalSessionCalories: UILabel!
    
    let logOutPopUp = YesNo.instanceFromNib() as! YesNo
    
    var arrData = [SideMenuData(icon: UIImage(named : "icon1")!, title: "Home"),
                   SideMenuData(icon: UIImage(named : "icon0")!, title: "Profile"),
                   SideMenuData(icon: UIImage(named : "icon1")!, title: "Activity"),
                   SideMenuData(icon: UIImage(named : "icon2")!, title: "Leaderboard"),
                   SideMenuData(icon: UIImage(named : "body_fat_percentage")!, title: "Personal Fitness History"),
                   SideMenuData(icon: UIImage(named : "icon3")!, title: "Rewards"),
                   SideMenuData(icon: UIImage(named : "icon4")!, title: "Book Appointments"),
                   SideMenuData(icon: UIImage(named : "icon7")!, title: "Franchise Info"),
                   SideMenuData(icon: UIImage(named : "icon0")!, title: "Blog"),
                   SideMenuData(icon: UIImage(named : "icon5")!, title: "Settings"),
                   SideMenuData(icon: UIImage(named : "icon6")!, title: "Sign out")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.updateSideMenuOptions()
        self.addNotificationObservers()
    }
    
    private func updateSideMenuOptions() {
        if let isAdmin = AppStateManager.sharedInstance.loggedInUser.isAdmin {
            if isAdmin.elementsEqual("yes") {
                self.arrData.insert(SideMenuData(icon: UIImage(named: "icon1")!, title: "Dashboard"), at: 1)
            }
        }
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.willShowSideMenu),
                                               name: NSNotification.Name(rawValue: NotificationConstants.WILL_SHOW_SIDE_MENU),
                                               object: nil)
    }
    
    @objc private func willShowSideMenu() {
        self.lblUserName.text = AppStateManager.sharedInstance.loggedInUser.loginId
        self.getCaloriesStats()
    }
}

//MARK:- Helper Methods
extension SideMenu{
    
    private func showViewController(withIdentifier identifier: String) {
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: identifier)
        let navController = UINavigationController(rootViewController: controller)
        navController.navigationBar.isHidden = true
        
        self.sideMenuViewController.contentViewController = navController
        self.sideMenuViewController.hideViewController()
    }

    private func pushToHome(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Home"))
        controller.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = controller
        sideMenuViewController.hideViewController()
    }
    
    private func pushToActivity(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Activity"))
        controller.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = controller
        sideMenuViewController.hideViewController()
    }
    
    private func pushToRewards(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Rewards"))
        controller.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = controller
        sideMenuViewController.hideViewController()
    }
    
    private func pushToSettings(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Settings"))
        controller.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = controller
        sideMenuViewController.hideViewController()
    }
    
    private func pushToLeaderboard(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Leaderboard"))
        controller.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = controller
        sideMenuViewController.hideViewController()
    }
    
    private func pushToBodyFatPercentage(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "BodyFatPercentage")) 
        controller.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = controller
        sideMenuViewController.hideViewController()
    }
    
    private func pushToEditProfile(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Profile"))
        controller.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = controller
        sideMenuViewController.hideViewController()
    }
    
    private func pushToBookAppointments(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "BookAppointments"))
        controller.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = controller
        sideMenuViewController.hideViewController()
    }
    
    private func pushToFranchiseInfo(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        controller.contentTitle = "Franchise Info"
        controller.contentURL = Constants.franchiseURL
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = navigationController
        sideMenuViewController.hideViewController()
    }
    
    private func pushToSweatThon() {
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        controller.contentTitle = "Sweat-A-Thon"
        controller.contentURL = Constants.sweatThonURL
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = navigationController
        sideMenuViewController.hideViewController()
    }

    private func pushToBlogs(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        controller.contentTitle = "Blog"
        controller.contentURL = Constants.blogURL
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = navigationController
        sideMenuViewController.hideViewController()
    }
    
    private func showLogoutPopUp(){
        logOutPopUp.btnYes.setTitle("Logout", for: .normal)
        logOutPopUp.btnNo.setTitle("Cancel", for: .normal)
        logOutPopUp.btnYes.addTarget(self, action: #selector(self.processLogout), for: .touchUpInside)
        logOutPopUp.lblError.text = "Are you sure you want to logout?"
        logOutPopUp.frame = (view.frame)
        logOutPopUp.showAnimate()
        self.sideMenuViewController.view.addSubview(logOutPopUp)
    }
    
}
extension SideMenu: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC", for: indexPath) as! SideMenuTVC
        cell.setData(self.arrData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = self.arrData[indexPath.row].title
        switch title {
        case "Home":
            self.showViewController(withIdentifier: "Home")
            break
        case "Dashboard":
            self.showViewController(withIdentifier: "FranchiseListViewController")
            break
        case "Profile":
            self.showViewController(withIdentifier: "Profile")
            break
        case "Activity":
            self.showViewController(withIdentifier: "Activity")
            break
        case "Leaderboard":
            self.showViewController(withIdentifier: "Leaderboard")
            break
        case "Personal Fitness History":
            self.showViewController(withIdentifier: "BodyFatPercentage")
            break
        case "Rewards":
            self.showViewController(withIdentifier: "Rewards")
            break
        case "Settings":
            self.showViewController(withIdentifier: "Settings")
            break
        case "Book Appointments":
            self.pushToBookAppointments()
            break
        case "Franchise Info":
            self.pushToFranchiseInfo()
            break
        case "Blog":
            self.pushToBlogs()
            break
        case "Sign out":
            self.showLogoutPopUp()
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

//MARK:- Webservices
extension SideMenu{
    @objc func processLogout(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        let params : [String:Any] = ["user_id":user_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.logout(params: params, success: { (responseObject) in
            Utility.hideLoader()
            AppStateManager.sharedInstance.logoutUser()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func getCaloriesStats() {
        let params: [String: Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.getCaloriesStats(params: params, success: { (responseObject) in
            if let totalSessions = responseObject["total_session"] as? String {
                self.lblTotalSessionCalories.text = totalSessions
            }
            
            if let recentCalories = responseObject["total_calories_burned"] as? String {
                self.lblRecentCalories.text = recentCalories
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
