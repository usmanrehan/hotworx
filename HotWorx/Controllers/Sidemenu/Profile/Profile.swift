//
//  Profile.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 09/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import SwiftDate
import ObjectMapper

class Profile: BaseController {
    @IBOutlet weak var lblUserName: UITextField!
    @IBOutlet weak var lblFirstName: UITextField!
    @IBOutlet weak var lblLastName: UITextField!
    @IBOutlet weak var lblEmail: UITextField!
    @IBOutlet weak var lblAddress: UITextField!
    @IBOutlet weak var lblGender: UITextField!
    @IBOutlet weak var lblDOB: UITextField!
    @IBOutlet weak var lblHeight: UITextField!
    @IBOutlet weak var lblWeight: UITextField!
    
    var isFromSideMenu = false
    let genderPickerValues = ["Male", "Female", "Not Specified"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
        self.setData()
        
        self.getUserProfile()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc private func handleDateOfBirthPicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        self.lblDOB.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func onBtnEdit(_ sender: UIButton) {
        self.processEditProfile()
    }
}

extension Profile: UIPickerViewDataSource, UIPickerViewDelegate {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.genderPickerValues.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.genderPickerValues[row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.lblGender.text = self.genderPickerValues[row]
    }
}

//MARK:- Helper Methods
extension Profile{
    private func setData() {
        self.lblEmail.text = AppStateManager.sharedInstance.loggedInUser.loginId ?? ""
        self.lblGender.text = AppStateManager.sharedInstance.loggedInUser.gender ?? ""
    }
    
    private func setData(_ user: UserProfile) {
        //self.lblUserName.text = user.fullName
        self.lblFirstName.text = user.firstName
        self.lblLastName.text = user.lastName
        self.lblGender.text = user.gender
        self.lblDOB.text = user.dateOfBirth
        self.lblHeight.text = user.height
        self.lblWeight.text = user.weight
    }
    
    private func setupUI() {
        let genderRightView = UIImageView(image: UIImage(named: "dropdown_icon"))
        genderRightView.frame = CGRect(x: 0, y: 0, width: 16, height: 16)
        genderRightView.contentMode = .scaleAspectFit
        self.lblGender.rightView = genderRightView
        self.lblGender.rightViewMode = .always
        
        self.setGenderPicker()
        
        let dobRightView = UIImageView(image: UIImage(named: "dropdown_icon"))
        dobRightView.frame = CGRect(x: 0, y: 0, width: 16, height: 16)
        dobRightView.contentMode = .scaleAspectFit
        self.lblDOB.rightView = dobRightView
        self.lblDOB.rightViewMode = .always
        
        self.setDateOfBirthPicker()
    }
    
    private func setGenderPicker() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        
        self.lblGender.inputView = pickerView
    }
    
    private func setDateOfBirthPicker() {
        let todayDate = DateInRegion() + 0.days
        
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.maximumDate = todayDate.date
        picker.addTarget(self, action: #selector(handleDateOfBirthPicker), for: .valueChanged)
        
        self.lblDOB.inputView = picker
    }
}

//MARK:-Service
extension Profile{
    private func processEditProfile(){
        Utility.showLoader()
        let param : [String: Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
            //"full_name": self.lblUserName.text ?? "",
            "first_name": self.lblFirstName.text ?? "",
            "last_name": self.lblLastName.text ?? "",
            "dob": self.lblDOB.text ?? "",
            "gender": self.lblGender.text ?? "",
            "height": self.lblHeight.text ?? "",
            "weight": self.lblWeight.text ?? "",
            "address": self.lblAddress.text ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.updateProfile(params: param, success: { (responseObject) in
            Utility.hideLoader()
            Utility.main.showAlert(message: "User profile updated", title: "Message")
            print(responseObject)
        }) { (error) in
            Utility.hideLoader()
            Utility.main.showAlert(message: error.localizedDescription, title: "Error")
        }
    }
    
    private func getUserProfile() {
        Utility.showLoader()
        let params: [String: Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        ]
        
        APIManager.sharedInstance.usersAPIManager.getUserProfile(params: params, success: { (responseObject) in
            Utility.hideLoader()
            if let data = responseObject["data"] {
                let userProfile = Mapper<UserProfile>().map(JSONObject: data)
                self.setData(userProfile!)
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.main.showAlert(message: error.localizedDescription, title: "Error")
        }
    }
}
