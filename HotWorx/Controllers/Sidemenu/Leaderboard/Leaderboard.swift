//
//  Leaderboard.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 06/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper

enum LeaderboardTypes {
    case user
    case customer
    case franchise
}

class Leaderboard: BaseController {
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var segmentControlHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.tableFooterView = UIView()
        }
    }
    
    var type: LeaderboardTypes = .user
    var locationId: String?
    
    private var dataSource: [AnyObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getLeaderboardData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func getLeaderboardData() {
        switch self.type {
        case .franchise:
            self.getFranchiseLeaderboard()
            self.segmentControlHeightConstraint.constant = 0
            break
        case .customer:
            self.getCustomerLeaderboard()
            self.segmentControlHeightConstraint.constant = 0
            break
        default:
            self.getUserLeaderboardGlobal()
            self.segmentControlHeightConstraint.constant = 40
            break
        }
    }
    
    @IBAction func onSegmentedControlChanges(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.getUserLeaderboardGlobal()
        } else {
            self.getUserLeaderboardLocal()
        }
    }
}

extension Leaderboard: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.arrLeaderBoard.count
        return self.dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderboardCell", for: indexPath)
            as! LeaderboardCell
        
        cell.setData(self.dataSource![indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let item = self.dataSource![indexPath.row] as? FranchiseLeaderboardModel {
            let leaderboardCell = cell as! LeaderboardCell            
            if (item.selfLocation?.elementsEqual("yes"))! {
                leaderboardCell.backgroundColor = UIColor(hexString: "#D67A2E", alpha: 0.75)
                leaderboardCell.lblUserName.textColor = .white
                leaderboardCell.lblTotalCaloriesBurnt.textColor = .white
                leaderboardCell.rewardLabel.textColor = .white
            } else {
                leaderboardCell.backgroundColor = .white
                leaderboardCell.lblUserName.textColor = .black
                leaderboardCell.lblTotalCaloriesBurnt.textColor = .black
                leaderboardCell.rewardLabel.textColor = .black
            }
        }
    }
}

//MARK:- Webservices
extension Leaderboard{
    private func getUserLeaderboardGlobal(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        let params : [String:Any] = ["user_id":user_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.leaderboardGlobal(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            let arrResponse = response.object(forKey: "leaderboard")
            let res = Mapper<LeaderboardObject>().mapArray(JSONArray: arrResponse as! [[String : Any]])
            self.dataSource = res
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func getUserLeaderboardLocal(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        let params : [String:Any] = ["user_id":user_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.leaderboardLocal(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            let arrResponse = response.object(forKey: "leaderboard")
            let res = Mapper<LeaderboardObject>().mapArray(JSONArray: arrResponse as! [[String : Any]])
            self.dataSource = res
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func getFranchiseLeaderboard() {
        Utility.showLoader()
        let params: [String: Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.getFranchiseLeaderbooard(params: params, success: { (response) in
            Utility.hideLoader()
            if let responseArray = response["leaderboard"] as? Array<[String: Any]> {
                let leaderboard = Mapper<FranchiseLeaderboardModel>().mapArray(JSONArray: responseArray)
                self.dataSource = leaderboard
                self.tableView.reloadData()
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(errorMessage: error.localizedDescription)
        }
    }
    
    private func getCustomerLeaderboard() {
        Utility.showLoader()
        let params: [String: Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
            "location_id": self.locationId ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.getFranchiseUsersLeaderbooard(params: params, success: { (response) in
            Utility.hideLoader()
            if let responseArray = response["leaderboard"] as? Array<[String: Any]> {
                let leaderboard = Mapper<LeaderboardObject>().mapArray(JSONArray: responseArray)
                self.dataSource = leaderboard
                self.tableView.reloadData()
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(errorMessage: error.localizedDescription)
        }
    }
}
