//
//  BookAppointments.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 09/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class BookAppointments: BaseController {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getOnlineAppointmentLink()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.sideMenuViewController.hideViewController()
    }    
}

extension BookAppointments: UIWebViewDelegate {
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    public func webViewDidStartLoad(_ webView: UIWebView) {
        Utility.showLoader()
        
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        Utility.hideLoader()
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Utility.hideLoader()        
        Utility.main.showAlert(message: error.localizedDescription, title: "Error")
    }
}

extension BookAppointments{
    private func getOnlineAppointmentLink(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        let action = WebRoute.GetOnlineAppointmentLink.rawValue
        let params : [String:Any] = ["user_id":user_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getOnlineAppointmentLink(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            guard let link = response.object(forKey: "contents") as? String else{return}
            let url = URL(string: link)
            if UIApplication.shared.canOpenURL(url!) && url != nil{
                self.webView.loadRequest(URLRequest(url: url!))
            }
            else{
                Utility.showAlert(errorMessage: "Provided link is not valid!")
            }
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}

