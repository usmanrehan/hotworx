//
//  PrivacyContentVC.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 07/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper

class PrivacyContentVC: BaseController {

    @IBOutlet weak var tvContent: UITextView!
    @IBOutlet weak var lblHeading: UILabel!
    var isPrivacyPolicy = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isPrivacyPolicy{
            self.getPrivacyContent()
        }
        else{
            self.lblHeading.text = "HELP"
            self.getHelpContent()
        }
    }
    @IBAction func onBtnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Webservice
extension PrivacyContentVC{
    private func getPrivacyContent(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        let action = WebRoute.GetPrivacyContent.rawValue
        let params : [String:Any] = ["user_id":user_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getPrivacyContent(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = Mapper<PrivacyContents>().map(JSON: responseObject as [String : Any])
            let content = PrivacyContents(value: response ?? PrivacyContents())
            self.tvContent.text = content.contents ?? "-"
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getHelpContent(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        let action = WebRoute.GetHelpContent.rawValue
        let params : [String:Any] = ["user_id":user_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getHelpContent(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = Mapper<PrivacyContents>().map(JSON: responseObject as [String : Any])
            let content = PrivacyContents(value: response ?? PrivacyContents())
            self.tvContent.text = content.contents ?? "-"
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
