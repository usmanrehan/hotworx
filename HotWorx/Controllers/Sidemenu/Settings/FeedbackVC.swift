//
//  FeedbackVC.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 07/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class FeedbackVC: BaseController {
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tvDescription: UITextView!
    
    let placeHolder = "Write here..."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvDescription.text = placeHolder
        self.tvDescription.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.validate()
    }
}
//MARK:- Helper Methods
extension FeedbackVC{
    private func validate(){
        let name = self.tfName.text!
        let email = self.tfEmail.text!
        let description = self.tvDescription.text!
        if name.count == 0 || email.count == 0 || description == self.placeHolder{
            Utility.showAlert(errorMessage: "All fields are required!")
            return
        }
        if Validation.isValidEmail(email) == false{
            Utility.showAlert(errorMessage: "Please provide a valid email!")
            return
        }
        self.addFeedback()
    }
    private func resetData(){
        self.tfName.text = ""
        self.tfEmail.text = ""
        self.tvDescription.text = placeHolder
        self.tvDescription.textColor = UIColor.lightGray
    }
}
//MARK:- UITextViewDelegate
extension FeedbackVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGray
        }
    }
}
//MARK:- Webservice
extension FeedbackVC{
    private func addFeedback(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        let name = self.tfName.text!.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "-")
        let email = self.tfEmail.text!
        let feedback = self.tvDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "-")
        let action = WebRoute.AddFeedback.rawValue
        let params : [String:Any] = ["user_id":user_id,"name":name,"email":email,"feedback":feedback]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.addFeedback(params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.resetData()
            Utility.showAlert(errorMessage: "Feedback submitted successfully!")
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
