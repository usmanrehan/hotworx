//
//  AboutApp.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 07/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class AboutApp: BaseController {
    @IBOutlet weak var textView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupAppVersion()
    }
    
    private func setupAppVersion() {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            let text = String(format: "%@\n\nApp Version: %@",self.textView.text, version)
            self.textView.text = text
        }
    }
    
    @IBAction func onBtnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
