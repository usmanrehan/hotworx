//
//  Settings.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 04/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class Settings: BaseController {

    let logOutPopUp = YesNo.instanceFromNib() as! YesNo
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.sideMenuViewController.hideViewController()
    }

    @IBAction func onBtnPersonalInfo(_ sender: UIButton) {
        //presentPersonalInfo()
    }
    
    @IBAction func onBtnAboutApp(_ sender: UIButton) {
        self.presentAboutApp()
    }
    
    @IBAction func onBtnFeedback(_ sender: UIButton) {
        self.presentFeedback()
    }
    
    @IBAction func onBtnHelp(_ sender: UIButton) {
        self.presentHelp()
    }
    
    @IBAction func onBtnPrivacyPolicy(_ sender: UIButton) {
        self.presentPrivacyPolicy()
    }
    
    @IBAction func onBtnSLogout(_ sender: UIButton) {
        self.showLogoutPopUp()
    }
    
}
//MARK:- Helper Methods
extension Settings{
    private func presentPersonalInfo(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
        controller.navigationController?.navigationBar.isHidden = true
        self.present(controller, animated: true, completion: nil)
        controller.isFromSideMenu = false
    }
    private func presentPrivacyPolicy(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "PrivacyContentVC") as! PrivacyContentVC
        controller.navigationController?.navigationBar.isHidden = true
        self.present(controller, animated: true, completion: nil)
    }
    private func presentHelp(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "PrivacyContentVC") as! PrivacyContentVC
        controller.navigationController?.navigationBar.isHidden = true
        self.present(controller, animated: true, completion: nil)
        controller.isPrivacyPolicy = false
    }
    private func presentAboutApp(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "AboutApp"))
        controller.navigationBar.isHidden = true
        self.present(controller, animated: true, completion: nil)
    }
    private func presentFeedback(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "FeedbackVC"))
        controller.navigationBar.isHidden = true
        self.present(controller, animated: true, completion: nil)
    }
    private func showLogoutPopUp(){
        logOutPopUp.btnYes.setTitle("Logout", for: .normal)
        logOutPopUp.btnNo.setTitle("Cancel", for: .normal)
        logOutPopUp.btnYes.addTarget(self, action: #selector(self.processLogout), for: .touchUpInside)
        logOutPopUp.lblError.text = "Are you sure you want to logout?"
        logOutPopUp.frame = (view.frame)
        logOutPopUp.showAnimate()
        self.view.addSubview(logOutPopUp)
    }
}
//MARK:- Webservices
extension Settings{
    @objc func processLogout(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        let action = WebRoute.Logout.rawValue
        let params : [String:Any] = ["user_id":user_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.logout(params: params, success: { (responseObject) in
            Utility.hideLoader()
            AppStateManager.sharedInstance.logoutUser()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
