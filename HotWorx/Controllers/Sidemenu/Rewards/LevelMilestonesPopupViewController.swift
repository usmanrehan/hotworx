//
//  LevelMilestonesPopupViewController.swift
//  HotWorx
//
//  Created by Akber Sayni on 11/10/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper

class LevelMilestonesPopupViewController: BaseController {
    
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblExpiryOn: UILabel!
    @IBOutlet weak var lockImageButton: UIButton!
    
    var objRedemptionSummary: RewardsRedemptionSummary?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.getRedemptionStatus()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func updateUI() {
        guard self.objRedemptionSummary != nil else { return }
        
        self.lblCalories.text = self.objRedemptionSummary?.calories ?? "-"
        self.lblStartDate.text = self.objRedemptionSummary?.startDate ?? "-"
        self.lblEndDate.text = self.objRedemptionSummary?.endDate ?? "-"
        
        if let lockStatus = self.objRedemptionSummary?.lockStatus,
            (lockStatus.caseInsensitiveCompare("open") == .orderedSame) {
            self.lockImageButton.isSelected = true
            self.lblExpiryOn.text = String(format: "Redeemed on: %@", self.objRedemptionSummary?.expiryDate ?? "-")
        } else {
            self.lockImageButton.isSelected = false
            self.lblExpiryOn.text = String(format: "Expires on: %@", self.objRedemptionSummary?.expiryDate ?? "-")
        }
        
        if let redeem = self.objRedemptionSummary?.allowRedeem,
            (redeem.caseInsensitiveCompare("no") == .orderedSame) {
            self.lockImageButton.isUserInteractionEnabled = false
        } else {
            self.lockImageButton.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func onBtnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnRedeem(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Confirmation", message: "Show this reward to sales associate to redeem your discount. Are you sure you want to redeem now?", preferredStyle: .alert)
        
        let actionYes = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.redeemReward()
        }
        
        let actionNo = UIAlertAction(title: "No", style: .cancel) { (action) in
            //Do nothing
        }
        
        alertController.addAction(actionYes)
        alertController.addAction(actionNo)
        
        self.present(alertController, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK: - Webservice API
extension LevelMilestonesPopupViewController {
    
    private func getRedemptionStatus() {
        Utility.showLoader()
        let params: [String: Any] = ["user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "0"]
        APIManager.sharedInstance.usersAPIManager.getRedemptionStatus(params: params, success: { (response) in
            Utility.hideLoader()
            if let data = response["ninetyDaysSummary"] {
                self.objRedemptionSummary = Mapper<RewardsRedemptionSummary>().map(JSONObject: data)
                self.updateUI()
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(errorMessage: error.localizedDescription)
        }
    }
    
    private func redeemReward() {
        Utility.showLoader()
        let params: [String: Any] = ["user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "0"]
        APIManager.sharedInstance.usersAPIManager.redeemReward(params: params, success: { (response) in
            Utility.hideLoader()
            self.dismiss(animated: true, completion: {
                Utility.main.showAlert(message: "Message", title: "Reward has been redeemed successfully")
            })
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(errorMessage: error.localizedDescription)
        }
    }
}
