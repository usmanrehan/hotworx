//
//  Rewards.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 09/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper

class Rewards: BaseController {
    
    @IBOutlet weak var progressView: VerticalProgressView!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblCurrentCalories: UILabel!
    @IBOutlet weak var lblCurrentLevels: UILabel!
    @IBOutlet weak var lblLevel1: UILabel!
    @IBOutlet weak var lblLevel2: UILabel!
    @IBOutlet weak var lblLevel3: UILabel!
    @IBOutlet weak var lblLevel4: UILabel!
    @IBOutlet weak var lblLevel5: UILabel!

    var objNinetyDaysSummary: NinetyDaysSummary? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.setupChartView()
        self.getNinetyDaysSummary()
        self.progressView.backgroundColor = UIColor.white
        self.progressView.trackTintColor = UIColor.white
        self.progressView.progressTintColor = Constants.THEME_ORANGE_COLOR
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.sideMenuViewController.hideViewController()
    }
    
    private func updateLabels() {
        guard self.objNinetyDaysSummary != nil else { return }
        
        self.lblStartDate.text = self.objNinetyDaysSummary?.startDate
        self.lblDays.text = self.objNinetyDaysSummary?.totalDays
        self.lblEndDate.text = self.objNinetyDaysSummary?.endDate
        self.lblCurrentCalories.text = self.objNinetyDaysSummary?.currentCalories
        self.lblCurrentLevels.text = self.objNinetyDaysSummary?.currentLevel
    }
    
    private func updateLevelLabels(maxValue: Int, minValue: Int) {
        let valuePerLevel = (maxValue - minValue)/5
        
        self.lblLevel5.text = String(format: "- %d CAL", maxValue)
        self.lblLevel4.text = String(format: "- %d CAL", maxValue - valuePerLevel)
        self.lblLevel3.text = String(format: "- %d CAL", maxValue - (valuePerLevel * 2))
        self.lblLevel2.text = String(format: "- %d CAL", maxValue - (valuePerLevel * 3))
        self.lblLevel1.text = String(format: "- %d CAL", maxValue - (valuePerLevel * 4))
    }
    
    private func updateProgressValue(currentValue: Float, maxValue: Float) {
        self.progressView.setProgress(currentValue/maxValue, animated: true)
    }
    
    @IBAction func onBtnLevelMilestones(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LevelMilestonesPopupViewController")
        self.present(controller!, animated: true, completion: nil)
    }
}

//MARK:- Service
extension Rewards {
    
    private func getNinetyDaysSummary() {
        let userId = AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        let params: [String: Any] = ["user_id": userId]
        
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getNinetyDaysSummary(params: params, success: { (responseObject) in
            Utility.hideLoader()
            if let ninetyDaysSummary = responseObject["ninetyDaysSummary"] {
                self.objNinetyDaysSummary = Mapper<NinetyDaysSummary>().map(JSONObject: ninetyDaysSummary)
                
                self.updateLabels()
                
                self.updateLevelLabels(maxValue: Int(self.objNinetyDaysSummary?.maxValue ?? "0")!,
                                       minValue: Int(self.objNinetyDaysSummary?.minValue ?? "0")!)
                
                self.updateProgressValue(currentValue: Float(self.objNinetyDaysSummary?.currentCalories ?? "0")!,
                                         maxValue: Float(self.objNinetyDaysSummary?.maxValue ?? "0")!)
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(errorMessage: error.localizedDescription)
        }
    }
}
