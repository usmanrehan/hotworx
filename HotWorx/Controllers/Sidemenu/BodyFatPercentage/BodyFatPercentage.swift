//
//  BodyFatPercentage.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 15/10/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper
import Charts

class BodyFatPercentage: BaseController {

    @IBOutlet weak var lblTotalSessions: UILabel!
    @IBOutlet weak var lblWorkOutCalories: UILabel!
    @IBOutlet weak var lblAfterBurnCalories: UILabel!
    @IBOutlet weak var lblTotalCalories: UILabel!
    @IBOutlet weak var lblLastWeight: UILabel!
    @IBOutlet weak var lblLastBodyFat: UILabel!
    @IBOutlet weak var viewGraph: BarChartView!
    
    var summaryThirtyDays = SummaryThirtyDays()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setChartUI()
        self.getSummaryThirtyDays()
        self.getBodyFatGraph()
    }
    
    @IBAction func onBtnRecordWeight_BodyFat(_ sender: UIButton) {
        self.pushToRecordBodyfatPercent()
    }
}
//MARK:- Helper Methods
extension BodyFatPercentage{
    private func setChartUI(){
        //self.viewGraph.delegate = self
        self.viewGraph.xAxis.labelPosition = .bottom
        self.viewGraph.backgroundColor = .clear
    }
    private func setData(data:SummaryThirtyDays){
        self.lblTotalSessions.text = data.totalSessions ?? "-"
        self.lblWorkOutCalories.text = data.workoutCalorieBurned ?? "-"
        self.lblAfterBurnCalories.text = data.afterburnCalorieBurned ?? "-"
        self.lblTotalCalories.text = data.totalCalorieBurned ?? "-"
        self.lblLastWeight.text = data.lastWeightReading ?? "-"
        self.lblLastBodyFat.text = data.lastBodyFatReading ?? "-"
    }
    private func setGraphData(data:[BodyFatGraph]){
        var arrDays   = [String]()
        var arrWeight = [Double]()
        var arrFats   = [Double]()
        for (i,item) in data.enumerated(){
            let days   = "Day" + String(data.count - i)//String(item.day)
            let weight = Double((item.weight ?? "0")) ?? 0.0
            let fats   = Double((item.bodyFat ?? "0")) ?? 0.0
            arrDays.append(days)
            arrWeight.append(weight)
            arrFats.append(fats)
        }
        var weightEntries: [BarChartDataEntry] = []
        var fatEntries: [BarChartDataEntry] = []
        
        for i in 0..<data.count{
            let weight = BarChartDataEntry(x: Double(i), y: arrWeight[i])
            weightEntries.append(weight)
            let fats = BarChartDataEntry(x: Double(i), y: arrFats[i])
            fatEntries.append(fats)
        }
        
        let chartWeightDataSet = BarChartDataSet(values: weightEntries, label: "Weight")
        let orangeColor = UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)
        chartWeightDataSet.colors = [orangeColor]
        
        let chartFatsDataSet = BarChartDataSet(values: fatEntries, label: "Fats")
        let redColor = UIColor(red:0.93, green:0.11, blue:0.15, alpha:0.75)
        chartFatsDataSet.colors = [redColor]
        
        let chartData = BarChartData(dataSets: [chartWeightDataSet,chartFatsDataSet])
        self.viewGraph.data = chartData
        self.viewGraph.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        self.viewGraph.xAxis.granularity = 1
        self.viewGraph.xAxis.valueFormatter = IndexAxisValueFormatter(values:arrDays)
        self.viewGraph.chartDescription?.text = ""
    }
    private func pushToRecordBodyfatPercent(){
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecordBodyFatPercent") as? RecordBodyFatPercent else {return}
        controller.summaryThirtyDays = self.summaryThirtyDays
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//extension BodyFatPercentage:ChartViewDelegate{
//
//}
//MARK:- Services
extension BodyFatPercentage{
    private func getSummaryThirtyDays(){
        Utility.showLoader()
        let params : [String:Any] = [
            "user_id":AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.getSummaryThirtyDays(params: params, success: { (responseObject) in
            Utility.hideLoader()
            guard let response = responseObject["data"] as? [String : Any] else {return}
            guard let summary = Mapper<SummaryThirtyDays>().map(JSON: response) else {return}
            self.summaryThirtyDays = summary
            self.setData(data: summary)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getBodyFatGraph(){
        Utility.showLoader()
        let params : [String:Any] = [
            "user_id":AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.getBodyFatGraph(params: params, success: { (responseObject) in
            Utility.hideLoader()
            guard let response = responseObject["daily_bodyfat"] as? [[String : Any]] else {return}
            let res = Mapper<BodyFatGraph>().mapArray(JSONArray: response)
            self.setGraphData(data: res)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
