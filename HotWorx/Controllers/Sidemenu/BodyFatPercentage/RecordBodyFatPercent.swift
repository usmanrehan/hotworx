//
//  RecordBodyFatPercent.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 15/10/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
struct DietDescription {
    var desc: String
    var isSelected: Bool
}

class RecordBodyFatPercent: BaseController {
    
    @IBOutlet weak var tfWeight: UITextField!
    @IBOutlet weak var tfFats: UITextField!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgCalories: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var imagePicker: UIImagePickerController!
    var summaryThirtyDays = SummaryThirtyDays()
    var arrDietDescription:[DietDescription] = [DietDescription(desc: "My diet has remained the same", isSelected: false),
                                                DietDescription(desc: "I've consumed the less calories", isSelected: false),
                                                DietDescription(desc: "I've consumed more calories", isSelected: false),
                                                DietDescription(desc: "I've a well balanced diet with restricted calorie intake", isSelected: false)]
    var answer = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }

    @IBAction func onBtnCamera(_ sender: UIButton) {
        self.uploadCaloriesCountImage()
    }
    
    @IBAction func onBtnSaveBodyFatPercentageReading(_ sender: UIButton) {
        self.validate()
    }
}
//MARK:- Helper Methods
extension RecordBodyFatPercent{
    private func setData(){
        self.tfWeight.text = self.summaryThirtyDays.lastWeightReading ?? "0.0"
        self.tfFats.text = self.summaryThirtyDays.lastBodyFatReading ?? "0.0"
        self.lblDate.text = Utility.stringCustomCurrentDate(requiredFormat: "MM/dd/yyyy")
    }
    private func validate(){
        let weight = (self.tfWeight.text ?? "")
        if weight.isEmpty || !self.isValidInput(value: weight){
            Utility.main.showAlert(message: "Please provide your weight in pounds.", title: "Alert")
            return
        }
        let fats = (self.tfFats.text ?? "")
        if fats.isEmpty || !self.isValidInput(value: fats){
            Utility.main.showAlert(message: "Please provide your body fat percentage.", title: "Alert")
            return
        }
        if self.answer.isEmpty{
            Utility.main.showAlert(message: "Please choose the option that best describes your diet over the last 30 days.", title: "Alert")
            return
        }
        if self.imgCalories.image != nil{
            //Upload image
            self.uploadCaloriesImage()
        }
        else{
            //Upload data
            self.recordBodyFatWith(imagePath:"")
        }
    }
    private func isValidInput(value:String)->Bool{
        if let _ = Double(value){
            return true
        }
        return false
    }
}
extension RecordBodyFatPercent: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDietDescription.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecordBodyFatPercentCell", for: indexPath) as? RecordBodyFatPercentCell else {return UITableViewCell()}
        let obj = self.arrDietDescription[indexPath.row]
        cell.setData(isSelected: obj.isSelected, desc: obj.desc)
        return cell
    }
}
extension RecordBodyFatPercent: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<self.arrDietDescription.count{
            self.arrDietDescription[i].isSelected = false
        }
        self.arrDietDescription[indexPath.row].isSelected = true
        self.answer = self.arrDietDescription[indexPath.row].desc
        self.tableView.reloadData()
    }
}
//MARK:- Image picker
extension RecordBodyFatPercent{
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
        
    }
    func uploadCaloriesCountImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
//MARK: - UIImagePickerControllerDelegate
extension RecordBodyFatPercent: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgCalories.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Services
extension RecordBodyFatPercent{
    private func uploadCaloriesImage(){
        guard let image = self.imgCalories.image else {return}
        guard let uploaded_file = UIImageJPEGRepresentation(image, 0.1) else {return}
        guard let user_id = AppStateManager.sharedInstance.loggedInUser.userId else {return}
        let params:[String:Any] = ["user_id":user_id,"uploaded_file":uploaded_file]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.uploadImage(params: params, success: { (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            self.recordBodyFatWith(imagePath:"")
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func recordBodyFatWith(imagePath:String){
        guard let user_id = AppStateManager.sharedInstance.loggedInUser.userId else {return}
        let weight_in_pound = self.tfWeight.text ?? ""
        let body_fat_percentage = self.tfFats.text ?? ""
        let date = self.lblDate.text ?? ""
        let image = imagePath
        let answer = self.answer
        let params:[String:Any] = ["user_id":user_id,
                                   "weight_in_pound":weight_in_pound,
                                   "body_fat_percentage":body_fat_percentage,
                                   "date":date,
                                   "image":image,
                                   "answer":answer]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.recordBodyFat(params: params, success: { (responseObject) in
            Utility.hideLoader()
            Utility.main.showAlert(message: "Body fat percentage reading saved.", title: "Success", controller: self, usingCompletionHandler: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
