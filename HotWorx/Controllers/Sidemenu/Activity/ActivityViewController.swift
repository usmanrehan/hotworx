//
//  ActivityViewController.swift
//  HotWorx
//
//  Created by Akber Sayni on 11/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import Charts

class Activity: BaseController, ChartViewDelegate {

    @IBOutlet weak var chartView: BarChartView!
    @IBOutlet weak var lblTotalSessions: UILabel!
    @IBOutlet weak var lblTotalCalBurned: UILabel!
    @IBOutlet weak var Segment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    
    var arrYearlyGraph = [String]()
    var arrMonthlyCalories = List<CaloriesSession>()
    var arrNintyDaysCalories = List<CaloriesSession>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupChartView()
        self.viewUserActivityYearly()
    }
    
    private func setupChartView() {
        chartView.noDataText = "No data provided"
        chartView.delegate = self
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.granularity = 1
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:months)
        chartView.backgroundColor = UIColor.clear
    }
    
    func setChart(values: [Double]) {
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<values.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "")
        chartDataSet.colors = [UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
        
        let chartData = BarChartData(dataSet: chartDataSet)
        
        chartView.data = chartData
        chartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        chartView.chartDescription?.text = ""
    }

    @IBAction func onSegmentChange(_ sender: UISegmentedControl) {
        self.tableView.reloadData()
    }
    
    private func shareOnSocialMedia(_ activityId: String) {
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ShareOnSocialMedia") as! ShareOnSocialMedia
        controller.parentActivityId = activityId

        self.present(controller, animated: true, completion: nil)
    }
}

extension Activity: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Segment.selectedSegmentIndex == 0{
            return self.arrMonthlyCalories.count
        }
        else{
            return self.arrNintyDaysCalories.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityTVC", for: indexPath) as! ActivityTVC
        if Segment.selectedSegmentIndex == 0{
            cell.setData(data: self.arrMonthlyCalories[indexPath.row])
        }
        else{
            cell.setData(data: self.arrNintyDaysCalories[indexPath.row])
        }
        return cell
    }
}

extension Activity: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = CGFloat(40)
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if Segment.selectedSegmentIndex == 0 {
            self.shareOnSocialMedia(self.arrMonthlyCalories[indexPath.row].activityId!)
        } else {
            self.shareOnSocialMedia(self.arrNintyDaysCalories[indexPath.row].activityId!)
        }
    }
}

//MARK:- Webservice
extension Activity{
    private func viewUserActivityYearly(){
        let params : [String:Any] = ["user_id":AppStateManager.sharedInstance.loggedInUser.userId ?? ""]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.viewUserActivityYearly(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = Mapper<ActivityModel>().map(JSON: responseObject as [String : Any])
            self.arrMonthlyCalories = (response?.monthlyCaloriesSession)!
            self.arrNintyDaysCalories = (response?.nintyDaysCaloriesSession)!
            self.lblTotalSessions.text = response?.totalSession ?? "0"
            self.lblTotalCalBurned.text = "\(response?.totalCaloriesBurned ?? "0")Cal"
            
            if (response?.totalCaloriesBurnedYearlyGraph.count)! > 0 {
                let yearlyBurnedCalories = response?.totalCaloriesBurnedYearlyGraph.first
                
                var yValues = [Double]()
                yValues.append(Double(yearlyBurnedCalories?.january ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.feburay ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.march ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.april ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.may ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.june ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.july ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.august ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.september ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.october ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.november ?? "0")!)
                yValues.append(Double(yearlyBurnedCalories?.december ?? "0")!)
                
                self.setChart(values: yValues)
            }
            
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
