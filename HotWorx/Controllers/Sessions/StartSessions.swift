//
//  StartSessions.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 02/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import PieCharts
import HGCircularSlider
import UserNotifications
import HealthKit
import WatchConnectivity

enum WatchType: Int {
    case other = 0
    case apple
    case fitbit
}

class StartSessions: BaseController, SessionCommands {
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblWorkout: UILabel!
    @IBOutlet weak var lblTimeCounter1: UILabel!
    @IBOutlet weak var pieChart: RangeCircularSlider!
    @IBOutlet weak var pieBGView: RoundedView!
    @IBOutlet weak var pieBGWidthCons: NSLayoutConstraint!
    @IBOutlet weak var pieBGHeightCons: NSLayoutConstraint!
    @IBOutlet weak var btnStartTimer: UIButton!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var lblWatchReachibility: UILabel!
    @IBOutlet weak var caloriesStackView: UIStackView!
    
    let healthStore = HKHealthStore()
    var workoutConfiguration: HKWorkoutConfiguration!
    
    var validSession: WCSession?
    var wcSessionActivationCompletion : ((WCSession)->Void)?

    var activeSession = SessionsData()
    var isSessionCompleted = Bool()
    var sessionStartedTimeInterval = TimeInterval()
    var timer: Timer?
    var second = 0
    var totalTime = 0
    
    var startCalories: String = "0"
    var isSessionResumed: Bool = false
    
    var watchType: WatchType = .other
	
	var isSessionStarted: Bool = false
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
		self.setData()
		self.setPieChart()
		self.setupWorkoutSession()
		self.checkIfSessionResumed()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
	
    private func setupWorkoutSession() {
        switch self.watchType {
        case .apple:
            self.setupAppleWatchSession()
            break
        case .fitbit:
            self.setupFitbitWatchSession()
            break
        default:
            break
        }
    }
	
	private func checkIfSessionResumed() {
		if self.isSessionResumed == true {
			if self.activeSession.sessionStartedTimeInterval != 0 {
				let currentTimeInterval = Date.timeIntervalSinceReferenceDate
				
				self.sessionStartedTimeInterval = self.activeSession.sessionStartedTimeInterval
				self.second = Int(currentTimeInterval - self.sessionStartedTimeInterval)
				
				if self.second >= self.totalTime {
					self.stopSessionTimer()
				} else {
					self.btnStartTimer.isUserInteractionEnabled = false
					self.startTimer()
				}
			}
		} else {
			// Add workout session to the active list
			self.updateActiveWorkoutSession()
		}
	}
	
    private func updateActiveWorkoutSession() {
        let watchType = self.watchType.rawValue
        let timeInterval = self.sessionStartedTimeInterval
        
        AppStateManager.sharedInstance.setActiveSession(self.activeSession, watchType: watchType, sessionStartedTimeInterval: timeInterval)
    }
    
    private func setData() {
        let duration = Int(self.activeSession.duration ?? "0")
        //let duration = 2; // TODO -- need to uncomment above line
        self.totalTime = duration! * 60
        self.lblWorkout.text = self.activeSession.type
        self.lblDuration.text = String(format: "%02d Minutes", duration!)
        self.lblTimeCounter1.text = self.getTimeString(time: totalTime)
    }
    
    private func setPieChart() {
        pieChart.maximumValue = CGFloat(self.totalTime)
        pieChart.numberOfRounds = 1
        
        if self.second >= self.totalTime {
            pieChart.startPointValue = 0
            pieChart.endPointValue = CGFloat(self.totalTime)
            self.lblTimeCounter1.text = self.getTimeString(time: 0)
        } else {
            pieChart.startPointValue = 0
            pieChart.endPointValue = CGFloat(self.second)
            let duration = Int("\(self.totalTime - self.second)")
            self.lblTimeCounter1.text = self.getTimeString(time: duration ?? 0)
        }
    }
	
    private func scheduleLocalNotification() {
        let notification = UNMutableNotificationContent()
        notification.title = "HOTWORX"
        notification.subtitle = self.activeSession.type ?? "Workout"
        notification.body = "RECORD YOUR CALORIES NOW!"
        notification.sound = UNNotificationSound.default()
        
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(self.totalTime), repeats: false)
        let request = UNNotificationRequest(identifier: "hotworx_local_notification", content: notification, trigger: notificationTrigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}

//MARK:- Helper Methods

extension StartSessions {
    private func getTimeString(time:Int)->String{
        let minutes = time/60
        let seconds = time%60
        if minutes < 10 && seconds < 10{
            return "0\(minutes):0\(seconds)"
        }
        if minutes > 9 && seconds > 9{
            return "\(minutes):\(seconds)"
        }
        if seconds < 10 && minutes > 9{
            return "\(minutes):0\(seconds)"
        }
        if minutes < 10 && seconds > 9{
            return "0\(minutes):\(seconds)"
        }
        if minutes == 0 && seconds == 0{
            return "00:00"
        }
        return "\(minutes):\(seconds)"
    }
    
    private func navigateToManualCaloriesSubmit() {
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ManualCaloriesSubmitComplete") as! ManualCaloriesSubmitComplete
        controller.completedSession = self.activeSession
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
	private func popToSessionViewController() {
		let controller = self.storyboard?.instantiateViewController(withIdentifier: "Sessions") as! Sessions
		
		// get current view controllers in stack and replace them
		let viewControllers = self.navigationController!.viewControllers
		let newViewControllers = NSMutableArray()
		
		// preserve the root view controller
		newViewControllers.add(viewControllers[0])
		
		// add the new view controller
		newViewControllers.add(controller)
		
		// animatedly change the navigation stack
		self.navigationController?.setViewControllers(newViewControllers as! [UIViewController], animated: true)
	}

    private func shareOnSocialMedia(_ activityId: String) {
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ShareOnSocialMedia") as! ShareOnSocialMedia
        controller.parentActivityId = activityId
        
        self.navigationController?.present(controller, animated: true, completion: {
            self.navigationController?.popToRootViewController(animated: false)
        })
    }
}

//MARK:- IBAction Methods

extension StartSessions {
	@IBAction func onBtnSideMenu(_ sender: UIButton) {
		Utility.main.showAlert(message: "Do you want to cancel this session?", title: "Confirmation", controller: self)
		{ (actionYes, actionNo) in
			if actionYes != nil {
				self.cancelSession()
			}
		}
	}
	
	@IBAction func onBtnStartSession(_ sender: UIButton) {
		if self.isSessionCompleted == false {
			// Start session
			self.onStartWorkoutSession()
		} else {
			// End session
			self.onEndWorkoutSession()
		}
	}
	
	@IBAction func onBtnSyncCalories(_ sender: UIButton) {
		self.sendUpdateCaloriesEvent()
	}
	
    private func onStartWorkoutSession() {
        if self.watchType == .apple {
            self.startAppleWatchSession()
        } else {
            self.startSessionTimer()
        }
    }
    
    private func onEndWorkoutSession() {
        switch self.watchType {
        case .apple:
            self.recordAppleWatchCalories()
        case .fitbit:
            self.recordFitbitSessionCalories()
        default:
            self.navigateToManualCaloriesSubmit()
        }
    }
}

//MARK:- Timer methods

extension StartSessions {
    private func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(firedTimer), userInfo: nil, repeats: true)
    }
    
    private func stopTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    @objc
	private func firedTimer() {
        let currentTimeInterval = Date.timeIntervalSinceReferenceDate
        let elapsedTimeInterval = currentTimeInterval - sessionStartedTimeInterval
        self.second = Int(elapsedTimeInterval)
		
        if self.second >= self.totalTime {
            self.stopSessionTimer()
        } else {
            self.updateSessionTimer(elapsedTimeInterval)
        }
    }
	
	private func startSessionTimer() {
		self.sessionStartedTimeInterval = Date.timeIntervalSinceReferenceDate
		self.btnStartTimer.isUserInteractionEnabled = false
		self.scheduleLocalNotification()
		
		// Start session timer
		self.startTimer()
		
		// Update active session
		self.updateActiveWorkoutSession()
	}
	
	private func stopSessionTimer() {
		self.btnStartTimer.isUserInteractionEnabled = true
		self.btnStartTimer.isSelected = true
		self.isSessionCompleted = true
		self.setPieChart()
		
		self.stopTimer()
	}
	
	private func updateSessionTimer(_ elapsedTime: Double) {
		self.isSessionCompleted = false
		self.setPieChart()
	}
}

//MARK:- Apple watch methods

extension StartSessions {
    private func addNotificationsObservers() {
        NotificationCenter.default.addObserver(
            self, selector: #selector(type(of: self).dataDidFlow(_:)),
            name: .dataDidFlow, object: nil
        )
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(type(of: self).activationDidComplete(_:)),
            name: .activationDidComplete, object: nil
        )
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(type(of: self).reachabilityDidChange(_:)),
            name: .reachabilityDidChange, object: nil
        )
    }

    private func setupAppleWatchSession() {
        self.addNotificationsObservers()
        
        // Show calories stackview
        self.caloriesStackView.isHidden = false
        self.updateReachabilityColor()

		if self.isSessionResumed == false {
			// Start apple watch session to get parent Id
			self.startWorkoutSession(with: "0", sessionType: "applewatch")
			self.initAppleWatchWorkoutSession()
		}
		
		self.sendUpdateCaloriesEvent()
    }
    
    private func initAppleWatchWorkoutSession() {
        self.workoutConfiguration = HKWorkoutConfiguration()
        self.workoutConfiguration.locationType = .indoor
        
        if let activityType = self.activeSession.appleWatchType {
            self.workoutConfiguration.activityType = format(activityType: activityType)
        } else {
            self.workoutConfiguration.activityType = .walking
        }

        self.healthStore.startWatchApp(with: self.workoutConfiguration) { (success, error) in
            if success {
                print("Success")
            }
        }
    }
    
    private func startAppleWatchSession() {
        if self.isWCSessionReachable() {
            // Send start workout command to apple watch
            let workoutDuration = Double(self.activeSession.duration ?? "0")! * 60.0
            
            let message = [PayloadKey.startWorkout: workoutDuration]
            sendMessage(message)
            
            // Wait for the response from apple watch
            GTAlertCollection.shared.hostViewController = self
            GTAlertCollection.shared.presentActivityAlert(withTitle: "Please wait", message: "We are trying to connect with apple watch") { (success) in
                print("Activity alert presented")
            }
            
        } else {
            Utility.main.showAlert(message: "Error", title: "Apple watch session is not reachable")
        }
    }
    
    private func cancelAppleWatchSession() {
        if self.isWCSessionReachable() {
            // Send cancel workout command to apple watch
            let message = [PayloadKey.command: PayloadKey.cancelWorkout]
            self.sendMessage(message)
        }
    }
    
    private func recordAppleWatchCalories() {
        if self.isWCSessionReachable() {
            // Record calories from apple watch
            self.recordCalories()
        } else {
            // Record manual calories
            self.recordManualCalories()
        }
    }
    
    private func recordCalories() {
        let calories = self.caloriesLabel.text ?? "0"

		// Update workout session calories
		self.updateWorkoutSession(withCalories: calories)
		
        //Send command to apple watch to restart session
        let message = [PayloadKey.command: PayloadKey.recordCalories]
        self.sendMessage(message)
    }
    
    private func recordManualCalories() {
        let message = "It seems your connection with apple watch is not reachable. Would you like to enter calories manually or update calories with display value?"
        
        let alert = UIAlertController(title: "Confirmation", message: message, preferredStyle: .alert)
        
        let actionYes = UIAlertAction(title: "Enter Calories", style: .default) { (action) in
            self.navigateToManualCaloriesSubmit()
        }
        
        let actionNo = UIAlertAction(title: "Update Calories", style: .default) { (action) in
            self.recordCalories()
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(actionYes)
        alert.addAction(actionNo)
        alert.addAction(actionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }

    private func updateReachabilityColor() {
        let isReachable = self.isWCSessionReachable()
        self.lblWatchReachibility.backgroundColor = isReachable ? .green : .red
    }
    
    private func sendUpdateCaloriesEvent() {
        let message = [PayloadKey.command: PayloadKey.updateCalories]
        self.sendMessage(message)
    }
    
    private func isWCSessionReachable() -> Bool {
        var isReachable = false
        if WCSession.default.activationState == .activated {
            isReachable = WCSession.default.isReachable
        }
        
        return isReachable
    }
    
    //MARK:- Notificaiton selector methods
	
    @objc func activationDidComplete(_ notification: Notification) {
        // Update reachibility status color changes
        self.updateReachabilityColor()
    }
    
    @objc func reachabilityDidChange(_ notification: Notification) {
        // Update reachibility status color change
        self.updateReachabilityColor()
        self.sendUpdateCaloriesEvent()
    }
    
    @objc func dataDidFlow(_ notification: Notification) {
        guard let message = notification.object as? [String: Any] else { return }
        if let command = message[PayloadKey.command] as? String {
            switch command {
            case PayloadKey.startWatchWorkout:
				if self.isSessionStarted == false {
					self.startSessionTimer()
					self.isSessionStarted = true
					GTAlertCollection.shared.dismissAlert(completion: nil)
				}
				break
            default:
                print("Unhandle command: \(command)")
                break
            }
        }
        
        // Get burned calories from apple watch
        if let calories = message[PayloadKey.activeEnergyBurned] as? Double {
            self.caloriesLabel.text = String(format: "%.2f", calories)
        }
    }
}

//MARK:- Fitbit watch methods

extension StartSessions {
    private func setupFitbitWatchSession() {
		if self.isSessionResumed == false {
			// Start fitbit watch session to get parent Id
			self.startWorkoutSession(with: startCalories, sessionType: "fitbit")
		}
    }
    
    private func recordFitbitSessionCalories() {
        // Fitbit update calories
        _ = FitbitAPIManager.sharedInstance.fetchCalories(for: "today") { (responseObject, error) in
            if let response = responseObject as? [String: Any] {
                if let value = response["value"] as? String {
                    self.saveFitbitCaloriesPopup(value)
                } else {
                    Utility.showAlert(errorMessage: "Failed to get calories. please try again")
                }
            } else {
                if let error = error {
                    Utility.showAlert(errorMessage: error.localizedDescription)
                }
            }
        }
    }
    
    private func saveFitbitCaloriesPopup(_ calories: String) {
        let title = "Device Calories: \(calories)"
        let message = "Would you like to update your calories with fitbit device, or record calories manually."
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let actionYes = UIAlertAction(title: "Update", style: .default) { (action) in
			// Update workout session calories
			self.updateWorkoutSession(withCalories: calories)
        }
        
        let actionNo = UIAlertAction(title: "Manual Calories", style: .default) { (action) in
            self.navigateToManualCaloriesSubmit()
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(actionYes)
        alertController.addAction(actionNo)
        alertController.addAction(actionCancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

//MARK: - API Methods

extension StartSessions {
    private func cancelSession() {
        Utility.showLoader()
        let params : [String: Any] = [
			"user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
			"parent_activity_id": AppStateManager.sharedInstance.parentActivityId
		]
        APIManager.sharedInstance.usersAPIManager.cancelSession(params: params, success: { (responseObject) in
            Utility.hideLoader()

            // Stop session timer
            self.stopTimer()

            // Reset parent activity id
            AppStateManager.sharedInstance.parentActivityId = "0"
            AppStateManager.sharedInstance.activityId = "0"
            AppStateManager.sharedInstance.arrActiveSessions = nil
            
            // Cancel apple watch workout session
            if self.watchType == .apple {
                self.cancelAppleWatchSession()
            }
            
            // Delete active session
            AppStateManager.sharedInstance.deleteActiveSession(self.activeSession)
			
			// remove parent activity id from user defaults
			AppStateManager.sharedInstance.removeParentActivityId()
            
            self.navigationController?.popToRootViewController(animated: true)
            
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func startWorkoutSession(with calories: String, sessionType: String) {
        Utility.showLoader()
        let date = Utility.stringCustomCurrentDate(requiredFormat: "d MMM yyyy")
        let params : [String:Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
            "starting_calories": calories,
            "starting_workout_date": date,
            "workout_type": self.activeSession.type ?? "",
            "parent_activity_id": AppStateManager.sharedInstance.parentActivityId,
            "session_type": sessionType
        ]
        APIManager.sharedInstance.usersAPIManager.startSession(params: params, success: { (responseObject) in
            Utility.hideLoader()
            if let activityId = responseObject["activity_id"] as? Int {
                AppStateManager.sharedInstance.activityId = String(activityId)
                if AppStateManager.sharedInstance.parentActivityId == "0" {
                    AppStateManager.sharedInstance.parentActivityId = String(activityId)
					
					// Set parent activity id to user defaults
					let parentActivityId = String(activityId)
					AppStateManager.sharedInstance.setParentActivityId(parentActivityId)
                }
			} else {
				Utility.showAlert(errorMessage: "Something went wrong. please try again")
				
				// Delete active session
				AppStateManager.sharedInstance.deleteActiveSession(self.activeSession)

				self.navigationController?.popViewController(animated: true)
			}
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func updateWorkoutSession(withCalories calories: String) {
        Utility.showLoader()
        let date = Utility.stringCustomCurrentDate(requiredFormat: "d MMM yyyy")
        let params : [String:Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
            "forty_burnt": calories,
            "starting_workout_date": date,
            "workout_type": self.activeSession.type ?? "",
            "activity_id": AppStateManager.sharedInstance.activityId,
			"parent_activity_id": AppStateManager.sharedInstance.parentActivityId
        ]
        APIManager.sharedInstance.usersAPIManager.updateSession(params: params, success: { (responseObject) in
            Utility.hideLoader()
			if let isAfterBurnSession = self.activeSession.isAfterBurnSession,
				isAfterBurnSession.lowercased().elementsEqual("yes") {
				// After burn session completed
				self.shareOnSocialMedia(AppStateManager.sharedInstance.parentActivityId)
				
				//Reset parent activity ID and active session collections
				AppStateManager.sharedInstance.parentActivityId = "0"
				AppStateManager.sharedInstance.activityId = "0"
				AppStateManager.sharedInstance.arrActiveSessions = nil
				
				// remove parent activity id from user defaults
				AppStateManager.sharedInstance.removeParentActivityId()
				
			} else {
				self.popToSessionViewController()
			}

			// Remove session from active list
			AppStateManager.sharedInstance.deleteActiveSession(self.activeSession)
			
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
