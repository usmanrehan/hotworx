//
//  Sessions.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 02/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper
import NotificationCenter
import HealthKit
import WatchConnectivity

class Sessions: BaseController, FitbitAuthHandlerDelegate {
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var arrTodaysSessions = [SessionsData]()
    var selectedSession: SessionsData?
    var fitbitAuthHandler: FitbitAuthHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fitbitAuthHandler = FitbitAuthHandler(delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getTodaysSessions()
    }
    
    private func saveFitbitCaloriesPopup(_ calories: String) {
        let title = "Device calories: \(calories)"
        let message = "Would you like to update your calories with fitbit device, or record calories manually."

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let actionYes = UIAlertAction(title: "Update", style: .default) { (action) in
            self.pushToStartFitbitSession(calories: calories)
        }
        
        let actionNo = UIAlertAction(title: "Manual Calories", style: .default) { (action) in
            self.presentManualCaloriesSubmitVC()
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alertController.addAction(actionYes)
        alertController.addAction(actionNo)
        alertController.addAction(actionCancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
	
	private func deviceSetupConfirmationPopup() {
		let alertController = UIAlertController(title: "Device Setup",
												message: "Which type of fitness tracker are you using?",
												preferredStyle: .actionSheet)
		
		let actionWatch = UIAlertAction(title: "Apple Watch", style: .default) { (action) in
			//Select apple watch
			self.startAppleWatchSession()
		}
		
		let actionFitbit = UIAlertAction(title: "Fitbit Watch", style: .default) { (action) in
			//Select fitbit watch
			self.requestFitbitAuthorization()
		}
		
		let actionNoWatch = UIAlertAction(title: "Other Device", style: .default) { (action) in
			//Don't have watch
			self.presentManualCaloriesSubmitVC()
		}
		
		let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
			//User cancel action
		}
		
		alertController.addAction(actionWatch)
		alertController.addAction(actionFitbit)
		alertController.addAction(actionNoWatch)
		alertController.addAction(actionCancel)
		
		self.present(alertController, animated: true, completion: nil)
	}

    @IBAction func onBtnContinue(_ sender: UIButton) {
        guard self.arrTodaysSessions.count > 0 else {
            Utility.showAlert(errorMessage: "No session available for todays")
            return
        }
        
        guard self.selectedSession != nil else {
            Utility.showAlert(errorMessage: "Please select a session to proceed")
            return
        }
        
        //Update active sessions collections
        AppStateManager.sharedInstance.arrActiveSessions = self.arrTodaysSessions
        
        self.deviceSetupConfirmationPopup()
    }
    
    //MARK:- FitbitAuthHandlerDelegate

    func authorizationDidFinish(_ success: Bool) {
        guard let authToken = self.fitbitAuthHandler?.authenticateCode else {
            return
        }
        
        FitbitAPIManager.sharedInstance.authorize(with: authToken)
        
        _ = FitbitAPIManager.sharedInstance.fetchCalories(for: "today") { (responseObject, error) in
            if let response = responseObject as? [String: Any] {
                if let value = response["value"] as? String {
                    self.saveFitbitCaloriesPopup(value)
                } else {
                    Utility.showAlert(errorMessage: "Failed to get calories. please try again")
                }
            } else {
                if let error = error {
                    Utility.showAlert(errorMessage: error.localizedDescription)
                }
            }
        }
    }
	
	// MARK: - Navigation
	
	private func pushToAppleWatchStartSession() {
		let storyboard = AppStoryboard.Home.instance
		let controller = storyboard.instantiateViewController(withIdentifier: "StartSessions") as! StartSessions
		controller.watchType = .apple
		controller.activeSession = selectedSession!
		self.navigationController?.pushViewController(controller, animated: true)
	}
	
	private func pushToStartFitbitSession(calories: String) {
		let storyboard = AppStoryboard.Home.instance
		let controller = storyboard.instantiateViewController(withIdentifier: "StartSessions") as! StartSessions
		controller.watchType = .fitbit
		controller.startCalories = calories
		controller.activeSession = selectedSession!
		self.navigationController?.pushViewController(controller, animated: true)
	}
	
	private func presentManualCaloriesSubmitVC(){
		let storyboard = AppStoryboard.Home.instance
		let controller = storyboard.instantiateViewController(withIdentifier: "ManualCaloriesSubmitVC") as! ManualCaloriesSubmitVC
		controller.activeSession = selectedSession
		self.navigationController?.pushViewController(controller, animated: true)
	}

}

//MARK: - Helper Methods

extension Sessions {
    private func startAppleWatchSession() {
        guard WCSession.isSupported() else {
            Utility.showAlert(errorMessage: "Watch connectivity session is not supported")
            return
        }
        
        guard WCSession.default.isWatchAppInstalled, WCSession.default.isPaired else {
            Utility.showAlert(errorMessage: "Hotworx watch counter part app is not installed")
            return
        }
        
        guard WCSession.default.activationState == .activated else {
            Utility.showAlert(errorMessage: "Watch connectivity session is not active")
            return
        }
        
        self.requestAccessToHealthKit()
    }

	public func requestAccessToHealthKit() {
		let healthStore = HKHealthStore()
		
		let allTypes = Set([HKObjectType.workoutType(),
							HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!,
							HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceCycling)!,
							HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!,
							HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!])
		
		healthStore.requestAuthorization(toShare: allTypes, read: allTypes) { (success, error) in
			if success {
				DispatchQueue.main.sync {
					self.pushToAppleWatchStartSession()
				}
			} else {
				if let error = error {
					Utility.showAlert(errorMessage: "Failed to authorize healthkit request \(error.localizedDescription)")
				}
			}
		}
	}
	
	public func requestFitbitAuthorization() {
		self.fitbitAuthHandler?.login(fromParentViewController: self)
	}
}

//MARK: - UITableView DataSource Delegate

extension Sessions: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTodaysSessions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("SessionsTVC", owner: self, options: nil)?.first as! SessionsTVC
		cell.backgroundColor = .clear
        cell.setData(data: self.arrTodaysSessions[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in self.arrTodaysSessions{
            i.isSelected = false
        }
        
        self.arrTodaysSessions[indexPath.row].isSelected = true
        self.selectedSession = self.arrTodaysSessions[indexPath.row]
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = 60
        return CGFloat(height)
    }
}

//MARK:- API Methods

extension Sessions {
    private func getTodaysSessions() {
        Utility.showLoader()
        let params:[String:Any] = [
			"user_id":AppStateManager.sharedInstance.loggedInUser.userId ?? "",
			"parent_id":AppStateManager.sharedInstance.parentActivityId
		]
        APIManager.sharedInstance.usersAPIManager.getTodaysSessions(params: params, success: { (responseObject) in
            Utility.hideLoader()
            if let data = responseObject["data"] as? [[String : Any]] {
                self.arrTodaysSessions.removeAll()
                for res in data{
                    let obj = SessionsData(JSON: res)
                    obj?.isSelected = false
                    self.arrTodaysSessions.append(obj!)
                }
                
                self.tableView.reloadData()
            }
            
            if let sixtyMinTime = responseObject["sixty_min_time"] as? String {
                Constants.AFTER_BURN_TIME_OUT = sixtyMinTime
            }
            
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
