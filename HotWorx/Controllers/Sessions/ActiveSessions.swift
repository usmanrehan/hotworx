//
//  ActiveSessions.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 03/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import HGCircularSlider

class ActiveSessions: BaseController {
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblSessionType: UILabel!
    @IBOutlet weak var lblTotalTime: UILabel!
    @IBOutlet weak var lblCaloriesCount: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pieChart: RangeCircularSlider!
    @IBOutlet weak var pieBGView: RoundedView!
    @IBOutlet weak var pieBGWidthCons: NSLayoutConstraint!
    @IBOutlet weak var pieBGHeightCons: NSLayoutConstraint!
    
    var timer = Timer()
    var second = 1
    var totalTime = 20
    override func viewDidLoad() {
        super.viewDidLoad()
        setPieChartUI()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnSideMenu(_ sender: UIButton) {
        self.pushToSessions()
    }
}
extension ActiveSessions{
    private func setPieChartUI(){
        pieChart.backgroundColor = UIColor.clear
        pieChart.startThumbImage = nil
        pieChart.endThumbImage = nil
        pieChart.maximumValue = CGFloat(self.totalTime)
        pieChart.startPointValue = 1
        pieChart.endPointValue = CGFloat(self.second)
        pieChart.numberOfRounds = 1
    }
    
    private func pushToSessions(){
        let storyboard = AppStoryboard.Home.instance
        let controller = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Sessions"))
        controller.navigationBar.isHidden = true
        sideMenuViewController?.contentViewController = controller
    }
}
extension ActiveSessions: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("SessionsTVC", owner: self, options: nil)?.first as! SessionsTVC
        return cell
    }
}
extension ActiveSessions: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = 60
        return CGFloat(height)
    }
}
