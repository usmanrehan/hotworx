//
//  StartAppleWatchSessionViewController.swift
//  HotWorx
//
//  Created by Akber Sayni on 09/09/2019.
//  Copyright © 2019 Gym. All rights reserved.
//

import UIKit
import HealthKit
import PieCharts
import HGCircularSlider
import WatchConnectivity
import UserNotifications

class StartAppleWatchSessionViewController: UIViewController {
	@IBOutlet weak var lblDuration: UILabel!
	@IBOutlet weak var lblWorkout: UILabel!
	@IBOutlet weak var lblTimeCounter1: UILabel!
	@IBOutlet weak var lblWatchReachibility: UILabel!
	@IBOutlet weak var pieChart: RangeCircularSlider!
	@IBOutlet weak var btnStartTimer: UIButton!
	@IBOutlet weak var caloriesStackView: UIStackView!

	let healthStore = HKHealthStore()
	var workoutConfiguration: HKWorkoutConfiguration!
	
	var activeSession = SessionsData()
	var sessionStartedTimeInterval = TimeInterval()
	var isSessionCompleted = Bool()
	var timer: Timer?
	var totalTime = 0
	var second = 0
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		self.addNotificationsObservers()
		self.updateReachabilityColor()
		
		setupAppleWatchSession()
		
		// Show calories stackview
		self.caloriesStackView.isHidden = false

		// Start apple watch session to get parent Id
		self.startSession(with: "0", sessionType: "applewatch")
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.setData()
		self.setPieChart()
	}
	
	private func setData() {
		let duration = Int(self.activeSession.duration ?? "0")
		self.totalTime = duration! * 60
		self.lblWorkout.text = self.activeSession.type
		self.lblDuration.text = String(format: "%02d Minutes", duration!)
		self.lblTimeCounter1.text = Utility.getFormattedTimeString(time: totalTime)
	}
	
	private func setPieChart() {
		self.pieChart.maximumValue = CGFloat(self.totalTime)
		self.pieChart.numberOfRounds = 1
		
		if self.second >= self.totalTime {
			pieChart.startPointValue = 0
			pieChart.endPointValue = CGFloat(self.totalTime)
			self.lblTimeCounter1.text = Utility.getFormattedTimeString(time: 0)
		} else {
			pieChart.startPointValue = 0
			pieChart.endPointValue = CGFloat(self.second)
			let duration = Int("\(self.totalTime - self.second)")
			self.lblTimeCounter1.text = Utility.getFormattedTimeString(time: duration ?? 0)
		}
	}
	
	private func setupAppleWatchSession() {
		self.workoutConfiguration = HKWorkoutConfiguration()
		self.workoutConfiguration.locationType = .indoor
		
		if let activityType = self.activeSession.appleWatchType {
			self.workoutConfiguration.activityType = format(activityType: activityType)
		} else {
			self.workoutConfiguration.activityType = .walking
		}
		
		self.healthStore.startWatchApp(with: self.workoutConfiguration) { (success, error) in
			if success {
				print("Success")
			}
		}
	}
	
	private func cancelAppleWatchSession() {
		if self.isWCSessionReachable() {
			// Send cancel workout command to apple watch
			let message = [PayloadKey.command: PayloadKey.cancelWorkout]
			self.sendMessage(message)
		}
	}
	
	private func updateReachabilityColor() {
		let isReachable = self.isWCSessionReachable()
		self.lblWatchReachibility.backgroundColor = isReachable ? .green : .red
	}
	
	private func isWCSessionReachable() -> Bool {
		var isReachable = false
		if WCSession.default.activationState == .activated {
			isReachable = WCSession.default.isReachable
		}
		
		return isReachable
	}
	
	private func scheduleLocalNotification() {
		let notification = UNMutableNotificationContent()
		notification.title = "HOTWORX"
		notification.subtitle = self.activeSession.type ?? "Workout"
		notification.body = "RECORD YOUR CALORIES NOW!"
		notification.sound = UNNotificationSound.default()
		
		let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(self.totalTime), repeats: false)
		let request = UNNotificationRequest(identifier: "hotworx_local_notification", content: notification, trigger: notificationTrigger)
		
		UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
	}

    // MARK: - Navigation
	
	private func popToSessionViewController() {
		for controller in (self.navigationController?.viewControllers)! {
			if controller is Sessions {
				self.navigationController?.popToViewController(controller, animated: true)
				return
			}
		}
		
		//If sessions view controller not found than set to root view controller
		self.navigationController?.popToRootViewController(animated: true)
	}

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}

extension StartAppleWatchSessionViewController {
	private func startTimer() {
		self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(firedTimer), userInfo: nil, repeats: true)
	}
	
	private func stopTimer() {
		if self.timer != nil {
			self.timer?.invalidate()
			self.timer = nil
		}
	}
	
	@objc private func firedTimer() {
		let currentTimeInterval = Date.timeIntervalSinceReferenceDate
		let elapsedTimeInterval = currentTimeInterval - sessionStartedTimeInterval
		self.second = Int(elapsedTimeInterval)		
		if self.second >= self.totalTime {
			self.stopSessionTimer()
		} else {
			self.updateSessionTimer(elapsedTimeInterval)
		}
	}
	
	private func startSessionTimer() {
		self.sessionStartedTimeInterval = Date.timeIntervalSinceReferenceDate
		self.btnStartTimer.isUserInteractionEnabled = false
		self.scheduleLocalNotification()
		
		self.startTimer()
	}
	
	private func stopSessionTimer() {
		self.btnStartTimer.isUserInteractionEnabled = true
		self.btnStartTimer.isSelected = true
		self.isSessionCompleted = true
		self.setPieChart()
		
		self.stopTimer()
	}
	
	private func updateSessionTimer(_ elapsedTime: Double) {
		self.isSessionCompleted = false
		self.setPieChart()
	}
}

//MARK: - API Methods
extension StartAppleWatchSessionViewController {
	private func cancelSession() {
		Utility.showLoader()
		let params : [String: Any] = ["user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? ""]
		APIManager.sharedInstance.usersAPIManager.cancelSession(params: params, success: { (responseObject) in
			Utility.hideLoader()
			
			// Stop session timer
			self.stopTimer()
			
			// Reset parent activity id
			AppStateManager.sharedInstance.parentActivityId = "0"
			AppStateManager.sharedInstance.activityId = "0"
			AppStateManager.sharedInstance.arrActiveSessions = nil
			
			// Cancel apple watch workout session
			self.cancelAppleWatchSession()

			// Delete active session
			AppStateManager.sharedInstance.deleteActiveSession(self.activeSession)
			
			self.navigationController?.popToRootViewController(animated: true)
			
		}) { (error) in
			Utility.hideLoader()
			print(error.localizedDescription)
		}
	}
	
	private func startSession(with calories: String, sessionType: String) {
		Utility.showLoader()
		let date = Utility.stringCustomCurrentDate(requiredFormat: "d MMM yyyy")
		let params : [String:Any] = [
			"user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
			"starting_calories": calories,
			"starting_workout_date": date,
			"workout_type": self.activeSession.type ?? "",
			"parent_activity_id": AppStateManager.sharedInstance.parentActivityId,
			"session_type": sessionType
		]
		APIManager.sharedInstance.usersAPIManager.startSession(params: params, success: { (responseObject) in
			Utility.hideLoader()
			if let activityId = responseObject["activity_id"] as? Int {
				AppStateManager.sharedInstance.activityId = String(activityId)
				if AppStateManager.sharedInstance.parentActivityId == "0" {
					AppStateManager.sharedInstance.parentActivityId = String(activityId)
				}
			}
		}) { (error) in
			Utility.hideLoader()
			print(error.localizedDescription)
		}
	}
	
	private func updateWorkoutSessionAPI(withCalories calories: String) {
		Utility.showLoader()
		let date = Utility.stringCustomCurrentDate(requiredFormat: "d MMM yyyy")
		let params : [String:Any] = [
			"user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
			"forty_burnt": calories,
			"starting_workout_date": date,
			"workout_type": self.activeSession.type ?? "",
			"activity_id": AppStateManager.sharedInstance.activityId
		]
		APIManager.sharedInstance.usersAPIManager.updateSession(params: params, success: { (responseObject) in
			Utility.hideLoader()
			if let activeSessions = AppStateManager.sharedInstance.arrActiveSessions, activeSessions.count > 1 {
				self.askToStartAnotherSession()
			} else {
				self.startOneHourAfterBurnSession()
			}
		}) { (error) in
			Utility.hideLoader()
			print(error.localizedDescription)
		}
	}
	
	private func updateAfterHourSessionAPI(withCalories calories: String) {
		Utility.showLoader()
		let date = Utility.stringCustomCurrentDate(requiredFormat: "d MMM yyyy")
		let params : [String:Any] = [
			"user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
			"sixty_burnt": calories,
			"starting_workout_date": date,
			"activity_id": AppStateManager.sharedInstance.parentActivityId
		]
		APIManager.sharedInstance.usersAPIManager.updateAfterHourSession(params: params, success: { (responseObject) in
			Utility.hideLoader()
			
			self.shareOnSocialMedia(AppStateManager.sharedInstance.parentActivityId)
			
			//Reset parent activity ID and active session collections
			AppStateManager.sharedInstance.parentActivityId = "0"
			AppStateManager.sharedInstance.activityId = "0"
			AppStateManager.sharedInstance.arrActiveSessions = nil
			
			AppStateManager.sharedInstance.deleteActiveSession(self.activeSession)
			
		}) { (error) in
			Utility.hideLoader()
			print(error.localizedDescription)
		}
	}
}


extension StartAppleWatchSessionViewController {
	private func addNotificationsObservers() {
		NotificationCenter.default.addObserver(
			self, selector: #selector(type(of: self).dataDidFlow(_:)),
			name: .dataDidFlow, object: nil
		)
		
		NotificationCenter.default.addObserver(
			self, selector: #selector(type(of: self).activationDidComplete(_:)),
			name: .activationDidComplete, object: nil
		)
		
		NotificationCenter.default.addObserver(
			self, selector: #selector(type(of: self).reachabilityDidChange(_:)),
			name: .reachabilityDidChange, object: nil
		)
	}
}
