//
//  SignIn.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 30/06/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper
import FRHyperLabel

class SignIn: BaseController {

    @IBOutlet weak var tfUserName_Email: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var lblBookAppointment: FRHyperLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupBookAppointmentAttributedString()
    }
    
    private func setupBookAppointmentAttributedString() {
        //Step 1: Define a normal attributed string for non-link texts
        let string = self.lblBookAppointment.text ?? ""
        
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                          NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: .footnote)]
        
        lblBookAppointment.attributedText = NSAttributedString(string: string, attributes: attributes)
        
        //Step 2: Define a selection handler block
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            let storyboard = AppStoryboard.Home.instance
            let controller = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            controller.contentTitle = "Book an Appointment"
            controller.contentURL = Constants.bookAppointmentLink
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        //Step 3: Add link substrings
        let linkAttributes = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: .subheadline),
                              NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
            as [NSAttributedStringKey : Any]
        
        lblBookAppointment.setLinkForSubstring("Book an Appointment", withAttribute: linkAttributes, andLinkHandler: handler)
    }
    
    @IBAction func onBtnSignIn(_ sender: UIButton) {
        if self.validateLoginData() {
            self.processSignIn()
        }
    }
}

//MARK:- Helper methods
extension SignIn {
    private func validateLoginData() -> Bool {
        guard !(self.tfUserName_Email.text?.isEmpty)! else {
            Utility.showAlert(errorMessage: "Required information not provided")
            return false
        }
        
        guard !(self.tfPassword.text?.isEmpty)! else {
            Utility.showAlert(errorMessage: "Required information not provided")
            return false
        }
        
        return true
    }
}

//MARK:- Service
extension SignIn{
    private func processSignIn(){
        let login_id = self.tfUserName_Email.text ?? ""
        let password = self.tfPassword.text ?? ""
        //let is_active = "1"
        let action = WebRoute.Login.rawValue
        let params : [String:Any] = ["action":action,
                                     "user_email":login_id,
                                     "user_phone":password,
                                     "apple_push_id": AppStateManager.sharedInstance.deviceToken]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.signIn(params:params, success: { (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user = User(value: response ?? User())
            try! Global.APP_REALM?.write(){
                AppStateManager.sharedInstance.loggedInUser = user
                Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: true)
            }
            
            AppDelegate.shared.changeRootViewController()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
