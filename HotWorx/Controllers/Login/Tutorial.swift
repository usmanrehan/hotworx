//
//  Tutorial.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 30/06/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
struct TutorialData{
    var imgTutorial = UIImage()
    var title = ""
    var description = ""
}

class Tutorial: BaseController {

    @IBOutlet weak var imgTutorial: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tvTitleDesc: UITextView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnNext: UIButton!
    
    let calorieBurnDescription = "Track your total calories burned during each HOTWORX session, as well as your 1-hour after-burn."
    let leaderboardDescription = "The leaderboard displays every user and their total calories burned for the year. The 1st place winner of this burn-off competition receives a special prize."
    let rewardsDescription = "Challenge yourself to burn as many calories as you can every 90 days and you'll cash in on some  awesome rewards!"
    
    var arrTutorialData = [TutorialData]()
    var tutorial_counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.arrTutorialData = [
        TutorialData(imgTutorial: #imageLiteral(resourceName: "icon_1"), title: "CALORIE BURN", description: calorieBurnDescription),
        TutorialData(imgTutorial: #imageLiteral(resourceName: "icon_2"), title: "LEADERBOARD", description: leaderboardDescription),
        TutorialData(imgTutorial: #imageLiteral(resourceName: "icon_3"), title: "REWARDS", description: rewardsDescription)
        ]
        
        self.setData()
    }
    
    @IBAction func onPageControl(_ sender: UIPageControl) {
        tutorial_counter = sender.currentPage
        self.imgTutorial.image = self.arrTutorialData[tutorial_counter].imgTutorial
        self.lblTitle.text = self.arrTutorialData[tutorial_counter].title
        self.tvTitleDesc.text = self.arrTutorialData[tutorial_counter].description
    }
    
    @IBAction func onBtnSkip(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "TutorialFinished")
        //AppDelegate.shared.changeRootViewController()
        AppDelegate.shared.showSignIn()
    }
    
    @IBAction func onBtnNext(_ sender: UIButton) {
        self.tutorial_counter += 1
        if tutorial_counter < self.arrTutorialData.count {
            self.pageControl.currentPage = self.tutorial_counter
            self.imgTutorial.image = self.arrTutorialData[tutorial_counter].imgTutorial
            self.lblTitle.text = self.arrTutorialData[tutorial_counter].title
            self.tvTitleDesc.text = self.arrTutorialData[tutorial_counter].description
        } else {
            UserDefaults.standard.set(true, forKey: "TutorialFinished")
            //AppDelegate.shared.changeRootViewController()
            AppDelegate.shared.showSignIn()
        }
    }
}

//MARK:- Helper Methos
extension Tutorial{
    private func setData(){
        self.imgTutorial.image = self.arrTutorialData.first?.imgTutorial
        self.lblTitle.text = self.arrTutorialData.first?.title
        self.tvTitleDesc.text = self.arrTutorialData.first?.description
    }
}
