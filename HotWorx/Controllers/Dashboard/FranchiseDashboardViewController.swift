//
//  FranchiseDashboardViewController.swift
//  HotWorx
//
//  Created by Akber Sayni on 17/07/2019.
//  Copyright © 2019 Gym. All rights reserved.
//

import UIKit
import PieCharts
import ObjectMapper

class FranchiseDashboardViewController: BaseController {
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var pieChart: PieChart!
    @IBOutlet weak var pieChartBG: UIView!
    @IBOutlet var dateButtons: [UIButton]!
    @IBOutlet weak var hitCaloriesLabel: UILabel!
    @IBOutlet weak var oneHourAferBurnLabel: UILabel!
    @IBOutlet weak var isometricCalorieLabel: UILabel!
    
    private var summaryDetails: Summary?
    
    var franchiseDetails: FranchiseModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setPieChartUI()
        self.getFranchiseSummary()
    }
    
    private func setPieChartUI() {
        self.pieChart.isUserInteractionEnabled = true
        
        self.pieChart.delegate = self
        self.pieChart.innerRadius = self.pieChart.frame.height * 0.275
        self.pieChart.outerRadius = self.pieChart.frame.height * 0.425
    }
    
    private func setSummaryData(_ data: FranchiseSummaryModel) {
        self.hitCaloriesLabel.text = data.hiitCalories ?? "0"
        self.oneHourAferBurnLabel.text = data.afterBurn ?? "0"
        self.isometricCalorieLabel.text = data.isometricCalories ?? "0"
    }
    
    private func setPieChartData(_ data: [WorkoutDetailsModel]) {
        var models = Array<PieSliceModel>()
        for item in data {
            let model = PieSliceModel(value: Double(item.calories ?? "0") ?? 0,
                                      color: UIColor(hexString: item.color ?? "#000000"),
                                      obj: item)
            
            models.append(model)
        }
        
        self.pieChart.clear()
        self.pieChart.models = Array()
        self.pieChart.models = models
        self.pieChartBG.isHidden = false
    }
    
    @IBAction func onBtnSelectDate(_ sender: UIButton) {
        for button in self.dateButtons {
            button.isSelected = false
        }
        
        sender.isSelected = true
        if let title = sender.titleLabel?.text, title.lowercased().elementsEqual("today") {
            self.getFranchiseSummary()
        } else {
            self.getFranchiseSummary(withInterval: sender.titleLabel?.text?.lowercased() ?? "")
        }
    }
    
    @IBAction func customerLeaderboardTapGesture(_ sender: UITapGestureRecognizer) {
        self.showLeaderboard(.customer)
    }
    
    @IBAction func franchiseLeaderboardTapGesture(_ sender: UITapGestureRecognizer) {
        self.showLeaderboard(.franchise)
    }
 
    private func getFranchiseSummary(withInterval interval: String = "dtd") {
        Utility.showLoader()
        let params: [String: Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "0",
            "location_id": self.franchiseDetails?.locationId ?? 0,
            "interval": interval
        ]
        APIManager.sharedInstance.usersAPIManager.getFranchiseSummary(params: params, success: { (response) in
            Utility.hideLoader()
            if let summary = response["summary"] as? [String: Any] {
                if let model = FranchiseSummaryModel(JSON: summary) {
                    self.setSummaryData(model)
                }
            }
            
            if let workout = response["workout_details"] as? Array<[String: AnyObject]> {
                let data = Mapper<WorkoutDetailsModel>().mapArray(JSONArray: workout)
                self.setPieChartData(data)
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(errorMessage: error.localizedDescription)
        }
    }
    
    // MARK: - Navigation
    
    private func showLeaderboard(_ type: LeaderboardTypes) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "Leaderboard")
            as! Leaderboard
        
        controller.type = type
        controller.locationId = self.franchiseDetails?.locationId
        
        self.navigationController?.pushViewController(controller, animated: true)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}

extension FranchiseDashboardViewController: PieChartDelegate {
    func onSelected(slice: PieSlice, selected: Bool) {
        if selected == true {
            if let data = slice.data.model.obj as? WorkoutDetailsModel {
                let message = String(format: "%@\nCalories:%@", data.type ?? "", data.calories ?? "")
                Utility.main.showAlert(message: message, title: "", controller: self) {
                    slice.view.selected = false
                }
            }
        }
    }
}
