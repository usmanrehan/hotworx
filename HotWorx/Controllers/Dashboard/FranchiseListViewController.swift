//
//  FranchiseListViewController.swift
//  HotWorx
//
//  Created by Akber Sayni on 15/07/2019.
//  Copyright © 2019 Gym. All rights reserved.
//

import UIKit

class FranchiseListViewController: BaseController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgGymCover: UIImageView!

    var dataSource = Array<FranchiseModel>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getFranchiseList()
    }
    
    
    private func getFranchiseList() {
        Utility.showLoader()
        let params: [String: Any] = [
            "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.getFranchiseList(params: params, success: { (response) in
            Utility.hideLoader()
            if let array = response["data"] as? [AnyObject] {
                for item in array {
                    let model = FranchiseModel(JSON: item as! [String: Any])
                    self.dataSource.append(model!)
                }
                
                self.tableView.reloadData()
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(errorMessage: error.localizedDescription)
        }
    }

    
    // MARK: - Navigation
    
    private func showDashboard(_ indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FranchiseDashboardViewController")
            as! FranchiseDashboardViewController
        
        controller.franchiseDetails = self.dataSource[indexPath.row]
        
        self.navigationController?.pushViewController(controller, animated: true)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}

extension FranchiseListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FranchiseListCellIdentifier", for: indexPath)
            as! FranchiseListViewCell
        
        cell.setData(self.dataSource[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.showDashboard(indexPath)
    }
}
