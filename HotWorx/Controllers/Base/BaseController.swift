//
//  BaseController.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit

class BaseController: UIViewController {
    @IBOutlet weak var backButton: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.updateBackButton()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func updateBackButton() {
        if let button = self.backButton {
            if self.navigationController?.viewControllers.count == 1 {
                button.setImage(UIImage(named: "hamBurger"), for: .normal)
                button.addTarget(self, action: #selector(sideMenuActionButton(_:)), for: .touchUpInside)
            } else {
                button.setImage(UIImage(named: "back_icon"), for: .normal)
                button.addTarget(self, action: #selector(backActionButton(_:)), for: .touchUpInside)
            }
        }
    }
    
    @IBAction func sideMenuActionButton(_ sender: UIButton) {
        sideMenuViewController.presentLeftMenuViewController()
    }
    
    @IBAction func backActionButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
        
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
