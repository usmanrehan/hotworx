//
//  Summary.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 16/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class ShareSummary: BaseController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


}
extension ShareSummary: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryTVC", for: indexPath)
        return cell
    }
}


extension ShareSummary: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = tableView.frame.height * 0.4725
        return height
    }
}
