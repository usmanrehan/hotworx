//
//  ShareOnSocialMedia.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 19/08/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper

class ShareOnSocialMedia: BaseController {
    @IBOutlet weak var tableView: UITableView!
	
    var arrWorkoutActivities = Array<CalorieBurntActivity>()
    var afterBurnCalories: String = "0"
    var parentActivityId = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.getFinalSummary(self.parentActivityId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.getSummary(with: Utility.stringCustomCurrentDate(requiredFormat: "yyyy-mm-dd"))
    }
    
    private func getImageOfTableView(_ view: UITableView) -> UIImage? {
        var image: UIImage? = nil
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: view.contentSize.width, height: view.contentSize.height), false, 0.0)
        
        let context = UIGraphicsGetCurrentContext()
        
        let previousFrame = view.frame
        view.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.contentSize.width, height: view.contentSize.height)
        view.layer.render(in: context!)
        view.frame = previousFrame;
        
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return image;
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnShareOnSocialMedia(_ sender: UIButton) {
        if let image = self.getImageOfTableView(self.tableView) {
            let imageToShare = [image]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            activityViewController.completionWithItemsHandler = {(activityType, completed, returnedItems, error) in
                if activityType == .postToFacebook, completed {
                    self.shareFacebookActivity(self.parentActivityId)
                }
            }
            
            self.present(activityViewController, animated: true, completion: nil)
            
        } else {
            Utility.showAlert(errorMessage: "Failed to get image. please try again")
        }
    }
}

//MARK:- UITableView DataSource Delegate
extension ShareOnSocialMedia: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrWorkoutActivities.count + 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryHeaderCellIdentifier")
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < self.arrWorkoutActivities.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell", for: indexPath) as! SummaryCell
            let workoutAcitivity = self.arrWorkoutActivities[indexPath.row]
            cell.lblWorkoutHeading.text = workoutAcitivity.activityName
            cell.lblWorkoutDuration.text = String(format: "%@ MINS", workoutAcitivity.activityTime ?? "0")
            cell.lblCaloriesCount.text = String(format: "%@ Calories", workoutAcitivity.calorieBurnt ?? "0") 
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell", for: indexPath) as! SummaryCell
            cell.lblWorkoutHeading.text = "AFTER BURN"
            cell.lblWorkoutDuration.text = String(format: "%@ MINS", Constants.AFTER_BURN_TIME_OUT)
            cell.lblCaloriesCount.text = String(format: "%@ Calories", self.afterBurnCalories)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
}

//MARK:- Webservices
extension ShareOnSocialMedia{
    private func getFinalSummary(_ activityId: String) {
        Utility.showLoader()
        let params : [String:Any] = ["user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
                                     "parent_activity_id": activityId]
        APIManager.sharedInstance.usersAPIManager.getFinalSummary(params: params, success: { (responseObject) in
            Utility.hideLoader()
            if let calorieDetails = responseObject["calorieDetails"] as? [String: Any] {
                if let sixtyBurnt = calorieDetails["sixty_burnt"] as? NSNumber {
                    self.afterBurnCalories = sixtyBurnt.stringValue
                }
                
                if let fourtyBurnt = calorieDetails["forty_burnt"] as? [[String : Any]] {
                    self.arrWorkoutActivities = Mapper<CalorieBurntActivity>().mapArray(JSONArray: fourtyBurnt)
                }
                
                self.tableView.reloadData()
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(errorMessage: error.localizedDescription)
        }
    }
    
    private func shareFacebookActivity(_ activityId: String) {
        let params: [String: Any] = ["user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "0",
                                     "parent_activity_id": activityId,
                                     "device":"IOS"]
        APIManager.sharedInstance.usersAPIManager.shareFacebookActivity(params: params, success: { (response) in
            print("Activity shared on facebook")
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
