//
//  ManualCaloriesSubmitComplete.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 09/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class ManualCaloriesSubmitComplete: BaseController {
	
	@IBOutlet weak var lblHeading: UILabel!
	@IBOutlet weak var lblWorkoutDuration: UILabel!
	@IBOutlet weak var lblWorkoutType: UILabel!
	@IBOutlet weak var tfEnterCalories: UITextField!
	@IBOutlet weak var lblDate: UILabel!
	@IBOutlet weak var imgCalories: UIImageView!
	
	var calorieImage: UIImage?
	
	var imagePicker: UIImagePickerController!
	var completedSession = SessionsData()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.setData()
	}
	
	@IBAction func onBtnCamera(_ sender: UIButton) {
		self.uploadCaloriesCountImage()
	}
	
	@IBAction func onBtnCompleteWorkout(_ sender: UIButton) {
		guard !((self.tfEnterCalories.text?.isEmpty)!) else {
			Utility.showAlert(errorMessage: "Please enter your calories reading")
			return
		}
		
		self.processEndSession()
	}
}

//MARK:- Helper Methods
extension ManualCaloriesSubmitComplete{
	private func setData(){
		self.lblWorkoutDuration.text = String(format: "%@ Minutes", completedSession.duration ?? "0")
		self.lblWorkoutType.text = completedSession.type
		self.lblDate.text = Utility.stringCustomCurrentDate(requiredFormat: "d MMM yyyy")
	}
	
	private func askToStartAnotherSession() {
		Utility.main.showAlert(message: "Would you like to start another session?", title: "Message", controller: self) { (actionYes, actionNo) in
			if actionYes != nil {
				//Start another session
				self.popToSessionViewController()
			} else {
				if actionNo != nil {
					//Start one hour after burn session
					self.startOneHourAfterBurnSession()
				}
			}
		}
	}
	
	private func shareOnSocialMedia(_ activityId: String) {
		let storyboard = AppStoryboard.Home.instance
		let controller = storyboard.instantiateViewController(withIdentifier: "ShareOnSocialMedia") as! ShareOnSocialMedia
		controller.parentActivityId = activityId
		
		self.navigationController?.present(controller, animated: true, completion: {
			self.navigationController?.popToRootViewController(animated: false)
		})
		
		//self.navigationController?.pushViewController(controller, animated: true)
	}
	
	private func popToSessionViewController() {
		let controller = self.storyboard?.instantiateViewController(withIdentifier: "Sessions") as! Sessions
		
		// get current view controllers in stack and replace them
		let viewControllers = self.navigationController!.viewControllers
		let newViewControllers = NSMutableArray()
		
		// preserve the root view controller
		newViewControllers.add(viewControllers[0])
		
		// add the new view controller
		newViewControllers.add(controller)
		
		// animatedly change the navigation stack
		self.navigationController?.setViewControllers(newViewControllers as! [UIViewController], animated: true)
	}
	
	private func startOneHourAfterBurnSession() {
		self.navigationController?.popToRootViewController(animated: false)
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationConstants.START_ONE_HOUR_AFTER_BURN), object: nil)
	}
}

extension ManualCaloriesSubmitComplete {
	private func processEndSession() {
		Utility.showLoader()
		var params : [String:Any] = [
			"user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
			"forty_burnt": self.tfEnterCalories.text ?? "",
			"starting_workout_date": self.lblDate.text ?? "",
			"workout_type": self.completedSession.type ?? "",
			"activity_id": AppStateManager.sharedInstance.activityId,
			"parent_activity_id": AppStateManager.sharedInstance.parentActivityId
		]
		
		if let image = self.calorieImage {
			let data = UIImageJPEGRepresentation(image, 0.7)
			params["starting_image"] = data ?? ""
		}
		
		APIManager.sharedInstance.usersAPIManager.updateSession(params: params, success: { (responseObject) in
			Utility.hideLoader()
			self.onSuccesResponse(responseObject)
		}) { (error) in
			Utility.hideLoader()
			print(error.localizedDescription)
		}
	}
	
	func onSuccesResponse(_ responseObject: [String: Any]) {
		if let isAfterBurnSession = self.completedSession.isAfterBurnSession,
			isAfterBurnSession.lowercased().elementsEqual("yes") {
			// After burn session ended
			self.shareOnSocialMedia(AppStateManager.sharedInstance.parentActivityId)
			
			//Reset parent activity ID and active session collections
			AppStateManager.sharedInstance.parentActivityId = "0"
			AppStateManager.sharedInstance.activityId = "0"
			AppStateManager.sharedInstance.arrActiveSessions = nil
			
			// remove parent activity id from user defaults
			AppStateManager.sharedInstance.removeParentActivityId()
			
		} else {
			self.popToSessionViewController()
		}
		
		// Remove active sesssion from list
		AppStateManager.sharedInstance.deleteActiveSession(self.completedSession)
	}
}

//MARK:- Image picker
extension ManualCaloriesSubmitComplete{
	func uploadFromCamera(){
		if UIImagePickerController.isSourceTypeAvailable(.camera) {
			imagePicker =  UIImagePickerController()
			imagePicker.delegate = self
			imagePicker.sourceType = .camera
			present(imagePicker, animated: true, completion: nil)
		}
	}
	
	func uploadFromGallery(){
		let picker = UIImagePickerController()
		picker.allowsEditing = true
		picker.delegate = self
		present(picker, animated: true, completion: nil)
	}
	
	func uploadCaloriesCountImage(){
		let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
		alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
			self.uploadFromCamera()
		}))
		alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
			self.uploadFromGallery()
		}))
		alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
		present(alert, animated: true, completion: nil)
	}
}

//MARK: - UIImagePickerControllerDelegate
extension ManualCaloriesSubmitComplete: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
			self.imgCalories.image = pickedImage
			self.calorieImage = pickedImage
		}
		
		self.dismiss(animated: true, completion: nil)
	}
}
