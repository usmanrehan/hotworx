//
//  ManualCaloriesSubmitVC.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 07/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class ManualCaloriesSubmitVC: BaseController {

    @IBOutlet weak var lblWorkOutName: UILabel!
    @IBOutlet weak var tfCaloriesCount: UITextField!
    @IBOutlet weak var lblStartEndReadings: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblImageName: UILabel!
    @IBOutlet weak var imgCalories: UIImageView!
    
    var calorieImage: UIImage?
    
    var imagePicker: UIImagePickerController!
    var activeSession: SessionsData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setData()
    }

    @IBAction func onBtnCamera(_ sender: UIButton) {
        self.uploadCaloriesCountImage()
    }
    
    @IBAction func onBtnStartREcording(_ sender: UIButton) {
        guard !((self.tfCaloriesCount.text?.isEmpty)!) else {
            Utility.showAlert(errorMessage: "Please enter your starting calories")
            return
        }
        
        self.processStartSession()
    }
}
extension ManualCaloriesSubmitVC {
    private func navigateToStartSession() {
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "StartSessions") as! StartSessions
        controller.activeSession = self.activeSession
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func setData() {
        self.lblDate.text = Utility.stringCustomCurrentDate(requiredFormat: "d MMM yyyy")
    }
}
extension ManualCaloriesSubmitVC{
    private func processStartSession(){
        if self.calorieImage != nil,
            let imageData = UIImageJPEGRepresentation(self.calorieImage!, 0.7) {
            Utility.showLoader()
            let params : [String:Any] = [
                "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
                "starting_calories": self.tfCaloriesCount.text ?? "",
                "starting_workout_date": self.lblDate.text ?? "",
                "workout_type": self.activeSession.type ?? "",
                "parent_activity_id": AppStateManager.sharedInstance.parentActivityId,
                "session_type": "manual",
                "starting_image": imageData]
            APIManager.sharedInstance.usersAPIManager.startSession(params: params, success: { (responseObject) in
                Utility.hideLoader()
                self.onSuccessResponse(responseObject)
            }) { (error) in
                Utility.hideLoader()
                print(error.localizedDescription)
            }
        } else {
            Utility.showLoader()
            let params : [String:Any] = [
                "user_id": AppStateManager.sharedInstance.loggedInUser.userId ?? "",
                "starting_calories": self.tfCaloriesCount.text ?? "",
                "starting_workout_date": self.lblDate.text ?? "",
                "workout_type": self.activeSession.type ?? "",
                "parent_activity_id": AppStateManager.sharedInstance.parentActivityId,
                "session_type": "manual"
            ]
            APIManager.sharedInstance.usersAPIManager.startSession(params: params, success: { (responseObject) in
                Utility.hideLoader()
                self.onSuccessResponse(responseObject)
            }) { (error) in
                Utility.hideLoader()
                print(error.localizedDescription)
            }
        }
    }
    
    private func onSuccessResponse(_ responseObject: [String: Any]) {
        if let activityId = responseObject["activity_id"] as? Int {
            AppStateManager.sharedInstance.activityId = String(activityId)
            if AppStateManager.sharedInstance.parentActivityId == "0" {
				let parentActivityId = String(activityId)
				AppStateManager.sharedInstance.parentActivityId = parentActivityId
				
				// Set parent activity id to user defaults
				AppStateManager.sharedInstance.setParentActivityId(parentActivityId)
				
            }
			
			self.navigateToStartSession()

		} else {
			Utility.showAlert(errorMessage: "Something went wrong. please try again")
		}
    }
}

//MARK:- Image picker
extension ManualCaloriesSubmitVC {
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
        
    }
    
    func uploadCaloriesCountImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

//MARK: - UIImagePickerControllerDelegate
extension ManualCaloriesSubmitVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgCalories.image = pickedImage
            self.calorieImage = pickedImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
