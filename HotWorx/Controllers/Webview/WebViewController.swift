//
//  WebViewController.swift
//  HotWorx
//
//  Created by Akber Sayni on 09/10/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class WebViewController: BaseController {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var contentTitle = ""
    var contentURL = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblTitle.text = self.contentTitle
        
        if let url = URL(string: self.contentURL) {
            self.webView.loadRequest(URLRequest(url: url))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WebViewController: UIWebViewDelegate {
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    public func webViewDidStartLoad(_ webView: UIWebView) {
        Utility.showLoader()
        
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        Utility.hideLoader()
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Utility.hideLoader()
        print(error.localizedDescription)
    }
}

