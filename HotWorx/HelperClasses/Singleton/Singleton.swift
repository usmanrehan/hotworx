//
//  Singleton.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation

class Singleton {
    static let sharedInstance = Singleton()
}
