//
//  AlertMessages.swift
//  The Court User
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation

enum AlertMessages: String {
    
    // MARK: NEXT MODULE
    case WillImplementInNextMode = "Functionality will be implemented in next module!"
    
    // MARK: API MANAGER
    case errorInternetConnection  = "Your Internet connection is not working properly or server is not responding, please retry."
    case errorInResponce           = "Server is not responding correctly, please retry."
    
    // MARK: Login and SignUp
    case nameNotValid = "Name should contain atleast 3 characters."
    case emailNotValid = "Please provide a valid Email Address."
    case pwdEmpty = "Please provide password."
    case confirmPwdEmpty = "Please confirm your password."
    case passwordNotValid = "Password should contain atleast 6 characters."
    case passwordInValidShort = "At least 6 characters."
    
    case passwordNotMatch = "New password and confirm password does not match."
    case passwordNotMatchShort = "Password does'nt match."
    
    case phoneNumber = "The phone number should be numeric with 7-15 digits."
    
    case AllFieldsRequired = "All Fields are required!"
    case LocationIsMandatory = "Please provide your location."
    case SelectSpecialization = "Please provide your specializations."
    case PreferredLanguage = "Please select your preferred language(s)"
    
    var message: String { return self.rawValue }
}
