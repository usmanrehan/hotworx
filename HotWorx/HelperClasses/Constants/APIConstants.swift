//
//  APIConstants.swift
//  HotWorx
//
//  Created by Akber Sayni on 10/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

enum WebRoute: String {
    case GetSummary = "get_summary"
    case GetFinalSummary = "get_final_summary"
    case GetCaloriesStats = "view_calorie_stats"
    case UpdateProfile = "update_profile"
    case viewProfile = "view_profile"
    case Login = "login"
    case Logout = "logout"
    case GetGlobalLeaderboard = "get_user_leaderboard_global"
    case GetLocalLeaderboard = "get_user_leaderboard_local"
    case GetPrivacyContent = "get_privacy_content"
    case GetHelpContent = "get_help_content"
    case AddFeedback = "add_feedback"
    case GetTodaysSessions = "get_todays_sessions"
    case AddStartSession = "add_start_session"
    case AddActiveSession = "add_active_session"
    case AddAfterHour = "add_after_hour"
    case GetOnlineAppointmentLink = "get_online_appointment_link"
    case GetAllRewards = "get_all_rewards"
    case CancelActivity = "cancel_activity"
    case viewUserActivityYearly = "view_user_activity_yearly"
    case GetNinetyDaysSummary = "get_ninety_days_summary"
    case GetSummaryThirtyDays = "get_summary_thirty_days"
    case GetBodyFatGraph = "get_bodayfat_graph"
    case UploadImage = "upload_image"
    case RecordBodyFat = "record_body_fat"
    case GetRedemptionStatus = "check_redemption_status"
    case RedeemReward = "mark_redemption"
    case ShareFacebookActivity = "share_activity_fb"
    case GetFranchiseList = "GetFranchiseList"
    case GetFranchiseSummary = "get_franchise_summary"
    case GetFranchiseLeaderboard = "get_franchise_leaderboard"
    case GetFranchiseUsersLeaderboard = "get_franchise_user_leaderboard"
}
