//
//  LeaderboardCell.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 06/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class LeaderboardCell: UITableViewCell {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTotalCaloriesBurnt: UILabel!
    @IBOutlet weak var imgReward: UIImageView!
    @IBOutlet weak var rewardLabel: UILabel!
    
    func setData(_ data: AnyObject) {
        if let model = data as? FranchiseLeaderboardModel {
            self.lblUserName.text = model.name
            self.lblTotalCaloriesBurnt.text = "\(model.calories ?? "0") Cal"
            if let reward = model.reward {
                switch reward {
                case "gold":
                    self.imgReward.image = #imageLiteral(resourceName: "gold_trophy")
                    self.rewardLabel.text = nil
                case "silver":
                    self.imgReward.image = #imageLiteral(resourceName: "silver_trophy")
                    self.rewardLabel.text = nil
                case "bronze":
                    self.imgReward.image = #imageLiteral(resourceName: "bronze_trophy")
                    self.rewardLabel.text = nil
                default:
                    self.imgReward.image = nil
                    self.rewardLabel.text = reward
                    break
                }
            }
        } else {
            if let model = data as? LeaderboardObject {
                self.lblUserName.text = model.username
                self.lblTotalCaloriesBurnt.text = "\(model.totalCaloriesBurnt ?? "0") Cal"
                self.rewardLabel.text = nil
                guard let reward = model.reward else { return }
                switch reward {
                case "gold":
                    self.imgReward.image = #imageLiteral(resourceName: "gold_trophy")
                case "silver":
                    self.imgReward.image = #imageLiteral(resourceName: "silver_trophy")
                case "bronze":
                    self.imgReward.image = #imageLiteral(resourceName: "bronze_trophy")
                default:
                    self.imgReward.image = nil
                    break
                }
            }
        }
    }
}
