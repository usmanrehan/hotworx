//
//  FranchiseListViewCell.swift
//  HotWorx
//
//  Created by Akber Sayni on 17/07/2019.
//  Copyright © 2019 Gym. All rights reserved.
//

import UIKit

class FranchiseListViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: FranchiseModel) {
        self.titleLabel.text = data.locationName
    }
}
