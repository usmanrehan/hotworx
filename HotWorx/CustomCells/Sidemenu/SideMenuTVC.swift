//
//  SideMenuTVC.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 01/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
struct SideMenuData {
    var icon = UIImage()
    var title = ""
}
class SideMenuTVC: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    func setData(_ data: SideMenuData){
        self.selectionStyle = .none
        self.imgIcon.image = data.icon
        self.lblTitle.text = data.title
    }
}
