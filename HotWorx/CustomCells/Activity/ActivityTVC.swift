//
//  ActivityTVC.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 11/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class ActivityTVC: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCalsBurned: UILabel!
    @IBOutlet weak var lblAfterBurned: UILabel!
    
    func setData(data:CaloriesSession){
        self.lblDate.text = data.date ?? "07/06/2018"
        self.lblCalsBurned.text = "\(data.caloriesFortySessionBurned ?? "0")cal"
        self.lblAfterBurned.text = "\(data.caloriesSixtySessionBurnedHour ?? "0")cal"
    }
}
