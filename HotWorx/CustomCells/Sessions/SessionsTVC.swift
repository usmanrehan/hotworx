//
//  SessionsTVC.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 02/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class SessionsTVC: UITableViewCell {
    
    @IBOutlet weak var btnRadioButton: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblSessionTitle: UILabel!
	@IBOutlet weak var afterBurnImageView: UIImageView!
    
    func setData(data: SessionsData){
        self.selectionStyle = .none
        self.lblDate.text = Utility.stringCustomCurrentDate(requiredFormat: "d MMM, yyyy")
        self.lblTime.text = "\(data.duration ?? "0") MIN"
        self.lblSessionTitle.text = data.type ?? "-"
        self.btnRadioButton.isSelected = data.isSelected
		
		if let isAfterBurnSession = data.isAfterBurnSession,
			isAfterBurnSession.lowercased().elementsEqual("yes") {
			self.afterBurnImageView.isHidden = false
			UIView.animate(withDuration: 1.0,
						   delay: 0,
						   options: [.repeat, .autoreverse],
						   animations: { self.afterBurnImageView.alpha = 0 }
			)
		} else {
			self.afterBurnImageView.isHidden = true
		}
    }
}
