//
//  SummaryCell.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 19/08/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell {
    @IBOutlet weak var lblCaloriesCount: UILabel!
    @IBOutlet weak var lblWorkoutHeading: UILabel!
    @IBOutlet weak var lblWorkoutDuration: UILabel!
    @IBOutlet weak var viewTimeDuration: RoundedView!
}
