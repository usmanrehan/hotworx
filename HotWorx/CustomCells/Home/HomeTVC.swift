//
//  HomeTVC.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 01/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class HomeTVC: UITableViewCell {
    
    @IBOutlet weak var outerCircle: RoundedView!
    @IBOutlet weak var innerCircle: RoundedView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var periodBottonConstraint: NSLayoutConstraint!
    
    func setData(data: ClassesCompleted){
        self.lblTitle.text = data.type ?? "-"
        self.lblDesc.text = "Burned \(data.burnCalories ?? "0") Calories"
        guard let type = data.type else{return}
        switch type {
        case "HOT ISO":
            self.outerCircle.backgroundColor = ColorConstants.YELLOW
            self.innerCircle.backgroundColor = ColorConstants.YELLOW
        case "HOT YOGA":
            self.outerCircle.backgroundColor = ColorConstants.BLUE
            self.innerCircle.backgroundColor = ColorConstants.BLUE
        case "HOT PILATES":
            self.outerCircle.backgroundColor = ColorConstants.RED
            self.innerCircle.backgroundColor = ColorConstants.RED
        default:
            self.outerCircle.backgroundColor = ColorConstants.RED
            self.innerCircle.backgroundColor = ColorConstants.RED
        }
    }
}
