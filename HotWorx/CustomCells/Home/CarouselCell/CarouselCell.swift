//
//  CarouselCell.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 01/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class CarouselCell: UIView {

    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var lblDate: UILabel!
    class func instanceFromNib() -> UIView
    {
        return UINib(nibName: "CarouselCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
