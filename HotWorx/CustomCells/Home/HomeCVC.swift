//
//  HomeCVC.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 01/07/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class HomeCVC: UICollectionViewCell {
    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    func setData(index:Int, data: [Summary]){
        switch index {
        case 0:
            self.lblCounter.text = data.first?.isometricCalories ?? "0"
            self.lblDesc.text = "Isometric Calories"
        case 1:
            self.lblCounter.text = data.first?.hiitCalories ?? "0"
            self.lblDesc.text = "HIIT Calories Burned"
        case 2:
            self.lblCounter.text = data.first?.afterBurn ?? "0"
            self.lblDesc.text = "1 Hour After Burn"
        default:
            break
        }
    }
}

