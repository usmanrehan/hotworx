//
//  RecordBodyFatPercentCell.swift
//  HotWorx
//
//  Created by M Usman Bin Rehan on 15/10/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class RecordBodyFatPercentCell: UITableViewCell {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnRadio: UIButton!
    
    func setData(isSelected:Bool,desc:String){
        self.selectionStyle = .none
        self.btnRadio.isSelected = isSelected
        self.lblDesc.text = desc
    }
}
