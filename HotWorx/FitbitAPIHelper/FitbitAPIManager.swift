//
//  FitbitAPIManager.swift
//  HotWorx
//
//  Created by Akber Sayni on 30/10/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit

class FitbitAPIManager: NSObject {
    
    static let sharedInstance: FitbitAPIManager = FitbitAPIManager()
    
    //static let baseAPIURL = URL(string:"https://api.fitbit.com/1")
    
    var session: URLSession?
    
    func authorize(with token: String) {
        let sessionConfiguration = URLSessionConfiguration.default
        var headers = sessionConfiguration.httpAdditionalHeaders ?? [:]
        headers["Authorization"] = "Bearer \(token)"
        sessionConfiguration.httpAdditionalHeaders = headers
        
        session = URLSession(configuration: sessionConfiguration)
    }
    
    func fetchCalories(for datePath: String, callback: @escaping (AnyObject?, Error?) -> Void) -> URLSessionDataTask? {
        guard let session = self.session else { return nil }
        
//        guard let caloriesURL = URL(string: "https://api.fitbit.com/1/user/-/activities/calories/date/today/today/1min.json") else { return nil }

        guard let caloriesURL = URL(string: "https://api.fitbit.com/1/user/-/activities/calories/date/today/today/1min.json") else { return nil }

        let dataTask = session.dataTask(with: caloriesURL) { (data, response, error) in
            guard let response = response as? HTTPURLResponse, response.statusCode < 300 else {
                DispatchQueue.main.async {
                    callback(nil, error)
                }
                
                return
            }
            
            guard let data = data,
                let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: AnyObject] else {
                    DispatchQueue.main.async {
                        callback(nil, error)
                    }
                    
                    return
            }
            
            print(dictionary)
            
            guard let calories = dictionary["activities-calories"] as? [[String: AnyObject]] else { return }

            DispatchQueue.main.async {
                callback(calories.first as AnyObject, nil)
            }
        }
        
        dataTask.resume()
        return dataTask
    }
}
