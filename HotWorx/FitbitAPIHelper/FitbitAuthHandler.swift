//
//  FitbitAuthHandler.swift
//  HotWorx
//
//  Created by Akber Sayni on 29/10/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import SafariServices

protocol FitbitAuthHandlerDelegate {
    func authorizationDidFinish(_ success :Bool)
}

class FitbitAuthHandler: NSObject, SFSafariViewControllerDelegate {
    
    let clientID = "22D4TF"
    let clientSecret = "43108e529fccd8ff1379cabf6b622565"
    let redirectURI = "hotworx://fitbit"
    let expiresIn = "604800"
    let authURL = URL(string: "https://www.fitbit.com/oauth2/authorize")
    let refreshTokenURL = URL(string: "https://api.fitbit.com/oauth2/token")
    let defaultScope = "sleep+settings+nutrition+activity+social+heartrate+profile+weight+location"

    var authenticateCode: String?
    var authorizationVC: SFSafariViewController?
    var delegate: FitbitAuthHandlerDelegate?
    
    init(delegate: FitbitAuthHandlerDelegate?) {
        super.init()
        self.delegate = delegate
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: NotificationConstants.launchNotification), object: nil, queue: nil) { [weak self] (notification: Notification) in
            //Parse and extract token
            let success: Bool
            if let token = self?.extractToken(notification, key: "#access_token") {
                self?.authenticateCode = token
                success = true
            } else {
                print("There was an error extracting the access token from the authentication response.")
                success = false
            }
            
            self?.authorizationVC?.dismiss(animated: true, completion: {
                self?.delegate?.authorizationDidFinish(success)
            })
        }
    }
    
    private func extractToken(_ notification: Notification, key: String) -> String? {
        guard let url = notification.userInfo?[UIApplicationLaunchOptionsKey.url] as? URL else {
            NSLog("notification did not contain launch options key with URL")
            return nil
        }
        
        // Extract the access token from the URL
        let strippedURL = url.absoluteString.replacingOccurrences(of: redirectURI, with: "")
        return self.parametersFromQueryString(strippedURL)[key]
    }
    
    // TODO: this method is horrible and could be an extension and use some functional programming
    private func parametersFromQueryString(_ queryString: String?) -> [String: String] {
        var parameters = [String: String]()
        if (queryString != nil) {
            let parameterScanner: Scanner = Scanner(string: queryString!)
            var name:NSString? = nil
            var value:NSString? = nil
            while (parameterScanner.isAtEnd != true) {
                name = nil;
                parameterScanner.scanUpTo("=", into: &name)
                parameterScanner.scanString("=", into:nil)
                value = nil
                parameterScanner.scanUpTo("&", into:&value)
                parameterScanner.scanString("&", into:nil)
                if (name != nil && value != nil) {
                    parameters[name!.removingPercentEncoding!]
                        = value!.removingPercentEncoding!
                }
            }
        }
        
        return parameters
    }
    
    // MARK: SFSafariViewControllerDelegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        self.delegate?.authorizationDidFinish(false)
    }
    
    // MARK: Public API
    
    public func login(fromParentViewController viewController: UIViewController) {
        guard let url = URL(string: "https://www.fitbit.com/oauth2/authorize?response_type=token&client_id="+clientID+"&redirect_uri="+redirectURI+"&scope="+defaultScope+"&expires_in=604800") else {
            NSLog("Unable to create authentication URL")
            return
        }
        
        let authorizationViewController = SFSafariViewController(url: url)
        authorizationViewController.delegate = self
        self.authorizationVC = authorizationViewController
        viewController.present(authorizationViewController, animated: true, completion: nil)
    }
    
    public static func logout() {
        // TODO
    }
}

