import Foundation
import UIKit

class YesNo: UIView
{
    @IBOutlet weak var alertView : UIView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    class func instanceFromNib() -> UIView
    {
        return UINib(nibName: "YesNo", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    override func layoutSubviews()
    {
        super.layoutSubviews()
    }
    @IBAction func onBtnNo(_ sender: Any) {
        self.removeAnimate()
    }
    // popup animation when appears
    func showAnimate()
    {
        self.alertView.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        self.alertView.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
        self.alertView.alpha = 1.0
        self.alertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    // popup animation when dissappears
    func removeAnimate()
    {
        self.alertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.25, animations: {
            self.alertView.alpha = 0.0
            self.alertView.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        }) { (success) in
            self.removeFromSuperview()
        }
    }
}





