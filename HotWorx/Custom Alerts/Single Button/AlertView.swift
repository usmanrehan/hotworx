import Foundation
import UIKit
protocol didTapOK {
    func onBtnOK()
}
class AlertView: UIView
{
    @IBOutlet weak var alertView : UIView!
    @IBOutlet weak var lblError: UILabel!
    var delegate : didTapOK?
    class func instanceFromNib() -> UIView
    {
        return UINib(nibName: "AlertView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    override func layoutSubviews()
    {
        super.layoutSubviews()
    }
    @IBAction func onBtnOk(_ sender: Any) {
        self.delegate?.onBtnOK()
        self.removeAnimate()
    }
    
    // popup animation when appears
    func showAnimate()
    {
        self.alertView.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        self.alertView.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
        self.alertView.alpha = 1.0
        self.alertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    // popup animation when dissappears
    func removeAnimate()
    {
        self.alertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.25, animations: {
            self.alertView.alpha = 0.0
            self.alertView.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        }) { (success) in
            self.removeFromSuperview()
        }
    }
}





