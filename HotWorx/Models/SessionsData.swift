//
//  Data.swift
//
//  Created by Hamza Hasan on 07/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class SessionsData: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let type = "type"
    static let duration = "duration"
    static let appleWatchType = "apple_watch_type"
    static let id = "id"
    static let isSelected = "isSelected"
    static let parentActivityId = "parent_activity_id"
	static let isAfterBurnSession = "is_after_burnt"
  }

  // MARK: Properties
    @objc dynamic var type: String? = ""
    @objc dynamic var duration: String? = ""
    @objc dynamic var appleWatchType: String? = ""
    @objc dynamic var id : Int = 0
    @objc dynamic var isSelected: Bool = false
    @objc dynamic var isAfterBurn: Bool = false
    @objc dynamic var parentActivityId: String? = "0"
    @objc dynamic var activityId: String? = "0"
	@objc dynamic var isAfterBurnSession: String?
    @objc dynamic var watchType: Int = WatchType.other.rawValue
    @objc dynamic var sessionStartedTimeInterval: TimeInterval = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    type <- map[SerializationKeys.type]
    duration <- map[SerializationKeys.duration]
    appleWatchType <- map[SerializationKeys.appleWatchType]
    id <- map[SerializationKeys.id]
    isSelected <- map[SerializationKeys.isSelected]
	isAfterBurnSession <- map[SerializationKeys.isAfterBurnSession]
    parentActivityId <- map[SerializationKeys.parentActivityId]
  }
}
