//
//  CalorieBurntActivity.swift
//  HotWorx
//
//  Created by Akber Sayni on 08/09/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class CalorieBurntActivity: Object, Mappable {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let activityName = "activity_name"
        static let calorieBurnt = "calorie_burnt"
        static let activityTime = "activity_time"
    }
    
    // MARK: Properties
    @objc dynamic var activityName: String? = ""
    @objc dynamic var calorieBurnt: String? = ""
    @objc dynamic var activityTime: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        activityName <- map[SerializationKeys.activityName]
        calorieBurnt <- map[SerializationKeys.calorieBurnt]
        activityTime <- map[SerializationKeys.activityTime]
    }
}
