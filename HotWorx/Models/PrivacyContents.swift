//
//  PrivacyContents.swift
//
//  Created by Hamza Hasan on 07/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class PrivacyContents: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let contents = "contents"
    static let message = "message"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var status = false
  @objc dynamic var contents: String? = ""
  @objc dynamic var message: String? = ""
  @objc dynamic var id : Int = 0
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    contents <- map[SerializationKeys.contents]
    message <- map[SerializationKeys.message]
    id <- map[SerializationKeys.id]
  }
}
