//
//  FranchiseSummaryModel.swift
//  HotWorx
//
//  Created by Akber Sayni on 18/07/2019.
//  Copyright © 2019 Gym. All rights reserved.
//

import UIKit
import ObjectMapper

class FranchiseSummaryModel: NSObject, Mappable {
    @objc dynamic var afterBurn: String?
    @objc dynamic var hiitCalories: String?
    @objc dynamic var isometricCalories: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }

    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        afterBurn <- map["after_burn"]
        hiitCalories <- map["hiit_calories"]
        isometricCalories <- map["isometric_calories"]
    }
}
