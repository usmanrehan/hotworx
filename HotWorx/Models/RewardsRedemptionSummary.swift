//
//  RewardsRedemptionSummary.swift
//  HotWorx
//
//  Created by Akber Sayni on 02/12/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class RewardsRedemptionSummary: Object, Mappable {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let startDate = "start_date"
        static let endDate = "end_date"
        static let calories = "calories"
        static let expiryDate = "expiry_date"
        static let lockStatus = "lock_status"
        static let allowRedeem = "allow_redeem"
    }
    
    // MARK: Properties
    @objc dynamic var startDate: String? = ""
    @objc dynamic var endDate: String? = ""
    @objc dynamic var calories: String? = ""
    @objc dynamic var expiryDate: String? = ""
    @objc dynamic var lockStatus: String? = ""
    @objc dynamic var allowRedeem: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        startDate <- map[SerializationKeys.startDate]
        endDate <- map[SerializationKeys.endDate]
        calories <- map[SerializationKeys.calories]
        expiryDate <- map[SerializationKeys.expiryDate]
        lockStatus <- map[SerializationKeys.lockStatus]
        allowRedeem <- map[SerializationKeys.allowRedeem]
    }
}
