//
//  ClassesCompleted.swift
//
//  Created by Hamza Hasan on 07/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ClassesCompleted: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let type = "type"
    static let burnCalories = "burn_calories"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var type: String? = ""
  @objc dynamic var burnCalories: String? = ""
  @objc dynamic var id : Int = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    type <- map[SerializationKeys.type]
    burnCalories <- map[SerializationKeys.burnCalories]
    id <- map[SerializationKeys.id]
  }


}
