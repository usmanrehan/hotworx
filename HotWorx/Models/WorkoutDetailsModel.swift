//
//  WorkoutDetailsModel.swift
//  HotWorx
//
//  Created by Akber Sayni on 17/07/2019.
//  Copyright © 2019 Gym. All rights reserved.
//

import UIKit
import ObjectMapper

class WorkoutDetailsModel: NSObject, Mappable {
    @objc dynamic var type: String? = ""
    @objc dynamic var calories: String? = ""
    @objc dynamic var color: String? = ""

    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        type <- map["type"]
        color <- map["color"]
        calories <- map["burn_calories"]
    }
}
