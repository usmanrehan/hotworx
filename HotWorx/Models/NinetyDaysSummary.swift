//
//  NinetyDaysSummary.swift
//  HotWorx
//
//  Created by Akber Sayni on 08/09/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class NinetyDaysSummary: Object, Mappable {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let startDate = "start_date"
        static let endDate = "end_date"
        static let totalDays = "total_days"
        static let currentCalories = "current_calories"
        static let thirtyDaysCalories = "thirty_days_calories"
        static let sixtyDaysCalories = "sixty_days_calories"
        static let maxValue = "max_value"
        static let minValue = "min_value"
        static let currentLevel = "current_level"
    }
    
    // MARK: Properties
    @objc dynamic var startDate: String? = ""
    @objc dynamic var endDate: String? = ""
    @objc dynamic var totalDays: String? = ""
    @objc dynamic var currentCalories: String? = ""
    @objc dynamic var thirtyDaysCalories: String? = ""
    @objc dynamic var sixtyDaysCalories: String? = ""
    @objc dynamic var maxValue: String?
    @objc dynamic var minValue: String?
    @objc dynamic var currentLevel: String?

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        startDate <- map[SerializationKeys.startDate]
        endDate <- map[SerializationKeys.endDate]
        totalDays <- map[SerializationKeys.totalDays]
        currentCalories <- map[SerializationKeys.currentCalories]
        thirtyDaysCalories <- map[SerializationKeys.thirtyDaysCalories]
        sixtyDaysCalories <- map[SerializationKeys.sixtyDaysCalories]
        maxValue <- map[SerializationKeys.maxValue]
        minValue <- map[SerializationKeys.minValue]
        currentLevel <- map[SerializationKeys.currentLevel]
    }
}
