//
//  UserProfile.swift
//  HotWorx
//
//  Created by Akber Sayni on 10/10/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class UserProfile: Object, Mappable {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let fullName = "full_name"
        static let firstName = "first_name"
        static let lastName = "last_name"
        static let email = "email"
        static let gender = "gender"
        static let dateOfBirth = "dob"
        static let height = "height"
        static let weight = "weight"
    }
    
    // MARK: Properties
    @objc dynamic var fullName: String? = ""
    @objc dynamic var firstName: String? = ""
    @objc dynamic var lastName: String? = ""
    @objc dynamic var email: String? = ""
    @objc dynamic var gender: String? = ""
    @objc dynamic var dateOfBirth: String? = ""
    @objc dynamic var height: String? = ""
    @objc dynamic var weight: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        fullName <- map[SerializationKeys.fullName]
        firstName <- map[SerializationKeys.firstName]
        lastName <- map[SerializationKeys.lastName]
        email <- map[SerializationKeys.email]
        gender <- map[SerializationKeys.gender]
        dateOfBirth <- map[SerializationKeys.dateOfBirth]
        height <- map[SerializationKeys.height]
        weight <- map[SerializationKeys.weight]
    }
}
