//
//  MonthlyCaloriesSession.swift
//
//  Created by Hamza Hasan on 11/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class CaloriesSession: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let activityId = "activity_id"
    static let date = "date"
    static let caloriesFortySessionBurned = "calories_forty_session_burned"
    static let caloriesSixtySessionBurnedHour = "calories_sixty_session_burned_hour"
  }

  // MARK: Properties
  @objc dynamic var activityId: String? = ""
  @objc dynamic var date: String? = ""
  @objc dynamic var caloriesFortySessionBurned: String? = ""
  @objc dynamic var caloriesSixtySessionBurnedHour: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "activityId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    activityId <- map[SerializationKeys.activityId]
    date <- map[SerializationKeys.date]
    caloriesFortySessionBurned <- map[SerializationKeys.caloriesFortySessionBurned]
    caloriesSixtySessionBurnedHour <- map[SerializationKeys.caloriesSixtySessionBurnedHour]
  }
}
