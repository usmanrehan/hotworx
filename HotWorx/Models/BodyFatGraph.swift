//
//  BodyFatGraph.swift
//
//  Created by Hamza Hasan on 15/10/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class BodyFatGraph: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bodyFat = "body_fat"
    static let weight = "weight"
    static let day = "day"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var bodyFat: String? = ""
  @objc dynamic var weight: String? = ""
  @objc dynamic var day : Int = 0
  @objc dynamic var id : Int = 0
    

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    bodyFat <- map[SerializationKeys.bodyFat]
    weight <- map[SerializationKeys.weight]
    day <- map[SerializationKeys.day]
    id <- map[SerializationKeys.id]
  }


}
