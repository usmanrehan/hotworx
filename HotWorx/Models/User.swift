//
//  User.swift
//
//  Created by Hamza Hasan on 06/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class User: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let status = "status"
        static let email = "email"
        static let password = "password"
        static let isActive = "is_active"
        static let gender = "gender"
        static let applePushId = "apple_push_id"
        static let userId = "user_id"
        static let loginId = "login_id"
        static let message = "message"
        static let isAdmin = "is_admin"
        static let franchiseCount = "franchise_count"
    }
    
    // MARK: Properties
    @objc dynamic var status = false
    @objc dynamic var email: String? = ""
    @objc dynamic var password: String? = ""
    @objc dynamic var isActive: String? = ""
    @objc dynamic var gender: String? = ""
    @objc dynamic var applePushId: String? = ""
    @objc dynamic var userId: String? = ""
    @objc dynamic var loginId: String? = ""
    @objc dynamic var message: String? = ""
    @objc dynamic var isAdmin: String? = ""
    @objc dynamic var franchiseCount: Int = 0

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "userId"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        status <- map[SerializationKeys.status]
        email <- map[SerializationKeys.email]
        password <- map[SerializationKeys.password]
        isActive <- map[SerializationKeys.isActive]
        gender <- map[SerializationKeys.gender]
        applePushId <- map[SerializationKeys.applePushId]
        userId <- map[SerializationKeys.userId]
        loginId <- map[SerializationKeys.loginId]
        message <- map[SerializationKeys.message]
        isAdmin <- map[SerializationKeys.isAdmin]
        franchiseCount <- map[SerializationKeys.franchiseCount]
    }    
}
