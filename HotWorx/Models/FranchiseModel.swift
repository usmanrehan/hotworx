//
//  FranchiseModel.swift
//  HotWorx
//
//  Created by Akber Sayni on 15/07/2019.
//  Copyright © 2019 Gym. All rights reserved.
//

import UIKit
import ObjectMapper

class FranchiseModel: NSObject, Mappable {
    @objc dynamic var locationId: String? = ""
    @objc dynamic var locationName: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        locationId <- map["location_id"]
        locationName <- map["location_name"]
    }
}
