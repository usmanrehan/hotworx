//
//  ActivityModel.swift
//
//  Created by Hamza Hasan on 11/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ActivityModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let totalSession = "total_session"
    static let totalCaloriesBurnedYearlyGraph = "total_calories_burned_yearly_graph"
    static let nintyDaysCaloriesSession = "ninty_days_calories_session"
    static let totalCaloriesBurned = "total_calories_burned"
    static let message = "message"
    static let monthlyCaloriesSession = "monthly_calories_session"
    static let id = "activity_id"
  }

  // MARK: Properties
  @objc dynamic var status = false
  @objc dynamic var totalSession: String? = ""
  @objc dynamic var totalCaloriesBurned: String? = ""
  @objc dynamic var message: String? = ""
  @objc dynamic var id: String? = ""
    
  var totalCaloriesBurnedYearlyGraph = List<YearlyBurnedCalories>()
  var nintyDaysCaloriesSession = List<CaloriesSession>()
  var monthlyCaloriesSession = List<CaloriesSession>()

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    message <- map[SerializationKeys.message]
    id <- map[SerializationKeys.id]
    totalSession <- map[SerializationKeys.totalSession]
    totalCaloriesBurned <- map[SerializationKeys.totalCaloriesBurned]
    monthlyCaloriesSession <- (map[SerializationKeys.monthlyCaloriesSession], ListTransform<CaloriesSession>())
    nintyDaysCaloriesSession <- (map[SerializationKeys.nintyDaysCaloriesSession], ListTransform<CaloriesSession>())
    totalCaloriesBurnedYearlyGraph <- (map[SerializationKeys.totalCaloriesBurnedYearlyGraph], ListTransform<YearlyBurnedCalories>())
  }
}
