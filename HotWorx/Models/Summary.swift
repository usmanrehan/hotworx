//
//  Summary.swift
//
//  Created by Hamza Hasan on 07/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Summary: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let afterBurn = "after_burn"
    static let isometricCalories = "isometric_calories"
    static let hiitCalories = "hiit_calories"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var afterBurn: String? = ""
  @objc dynamic var isometricCalories: String? = ""
  @objc dynamic var hiitCalories: String? = ""
  @objc dynamic var id : Int = 0
    
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    afterBurn <- map[SerializationKeys.afterBurn]
    isometricCalories <- map[SerializationKeys.isometricCalories]
    hiitCalories <- map[SerializationKeys.hiitCalories]
    id <- map[SerializationKeys.id]
  }


}
