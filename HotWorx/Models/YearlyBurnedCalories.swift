//
//  YearlyBurnedCalories.swift
//  HotWorx
//
//  Created by Akber Sayni on 22/09/2018.
//  Copyright © 2018 Gym. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class YearlyBurnedCalories: Object, Mappable {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let january = "January"
        static let feburay = "February"
        static let march = "March"
        static let april = "April"
        static let may = "May"
        static let june = "June"
        static let july = "July"
        static let august = "August"
        static let september = "September"
        static let october = "October"
        static let november = "November"
        static let december = "December"
    }
    
    // MARK: Properties
    @objc dynamic var january: String? = ""
    @objc dynamic var feburay: String? = ""
    @objc dynamic var march: String? = ""
    @objc dynamic var april: String? = ""
    @objc dynamic var may: String? = ""
    @objc dynamic var june: String? = ""
    @objc dynamic var july: String? = ""
    @objc dynamic var august: String? = ""
    @objc dynamic var september: String? = ""
    @objc dynamic var october: String? = ""
    @objc dynamic var november: String? = ""
    @objc dynamic var december: String? = ""

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        january <- map[SerializationKeys.january]
        feburay <- map[SerializationKeys.feburay]
        march <- map[SerializationKeys.march]
        april <- map[SerializationKeys.april]
        may <- map[SerializationKeys.may]
        june <- map[SerializationKeys.june]
        july <- map[SerializationKeys.july]
        august <- map[SerializationKeys.august]
        september <- map[SerializationKeys.september]
        october <- map[SerializationKeys.october]
        november <- map[SerializationKeys.november]
        december <- map[SerializationKeys.december]
    }
}
