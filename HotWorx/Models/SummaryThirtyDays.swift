//
//  SummaryThirtyDays.swift
//
//  Created by Hamza Hasan on 15/10/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class SummaryThirtyDays: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lastBodyFatReading = "last_body_fat_reading"
    static let lastWeightReading = "last_weight_reading"
    static let totalCalorieBurned = "total_calorie_burned"
    static let afterburnCalorieBurned = "afterburn_calorie_burned"
    static let workoutCalorieBurned = "workout_calorie_burned"
    static let totalSessions = "total_sessions"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var lastBodyFatReading: String? = ""
  @objc dynamic var lastWeightReading: String? = ""
  @objc dynamic var totalCalorieBurned: String? = ""
  @objc dynamic var afterburnCalorieBurned: String? = ""
  @objc dynamic var workoutCalorieBurned: String? = ""
  @objc dynamic var totalSessions: String? = ""
  @objc dynamic var id : Int = 0
    
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    lastBodyFatReading <- map[SerializationKeys.lastBodyFatReading]
    lastWeightReading <- map[SerializationKeys.lastWeightReading]
    totalCalorieBurned <- map[SerializationKeys.totalCalorieBurned]
    afterburnCalorieBurned <- map[SerializationKeys.afterburnCalorieBurned]
    workoutCalorieBurned <- map[SerializationKeys.workoutCalorieBurned]
    totalSessions <- map[SerializationKeys.totalSessions]
    id <- map[SerializationKeys.id]
  }
}
