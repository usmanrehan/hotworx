//
//  Type.swift
//
//  Created by Hamza Hasan on 09/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Type: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let gender = "gender"
    static let rewardDescription = "reward_description"
    static let reward = "reward"
    static let rewardId = "reward_id"
    static let isRedeemed = "is_redeemed"
  }

  // MARK: Properties
  @objc dynamic var gender: String? = ""
  @objc dynamic var rewardDescription: String? = ""
  @objc dynamic var reward: String? = ""
  @objc dynamic var rewardId: String? = ""
  @objc dynamic var isRedeemed: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "rewardId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    gender <- map[SerializationKeys.gender]
    rewardDescription <- map[SerializationKeys.rewardDescription]
    reward <- map[SerializationKeys.reward]
    rewardId <- map[SerializationKeys.rewardId]
    isRedeemed <- map[SerializationKeys.isRedeemed]
  }


}
