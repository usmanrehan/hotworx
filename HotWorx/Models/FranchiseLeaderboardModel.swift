//
//  FranchiseLeaderboardModel.swift
//  HotWorx
//
//  Created by Akber Sayni on 18/07/2019.
//  Copyright © 2019 Gym. All rights reserved.
//

import UIKit
import ObjectMapper

class FranchiseLeaderboardModel: NSObject, Mappable {
    @objc dynamic var name: String? = ""
    @objc dynamic var parent: String? = ""
    @objc dynamic var reward: String? = ""
    @objc dynamic var calories: String? = ""
    @objc dynamic var selfLocation: String? = ""

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        name <- map["name"]
        parent <- map["parent"]
        reward <- map["reward"]
        calories <- map["TotalCaloriesBurnt"]
        selfLocation <- map["self_location"]
    }
}
