//
//  Leaderboard.swift
//
//  Created by Hamza Hasan on 06/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class LeaderboardObject: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let totalCaloriesBurnt = "TotalCaloriesBurnt"
    static let userId = "user_id"
    static let reward = "reward"
    static let username = "username"
  }

  // MARK: Properties
  @objc dynamic var totalCaloriesBurnt: String? = ""
  @objc dynamic var userId: String? = ""
  @objc dynamic var reward: String? = ""
  @objc dynamic var username: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "userId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    totalCaloriesBurnt <- map[SerializationKeys.totalCaloriesBurnt]
    userId <- map[SerializationKeys.userId]
    reward <- map[SerializationKeys.reward]
    username <- map[SerializationKeys.username]
  }


}
